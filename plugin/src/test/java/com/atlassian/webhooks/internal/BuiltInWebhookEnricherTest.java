package com.atlassian.webhooks.internal;

import com.atlassian.webhooks.*;
import com.atlassian.webhooks.diagnostics.WebhookDiagnosticsEvent;
import com.atlassian.webhooks.internal.publish.DefaultWebhookInvocation;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsMapContaining.hasEntry;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BuiltInWebhookEnricherTest {

    @Test
    public void testIdIsAddedToPublishRequest() {
        WebhookRequestEnricher enricher = new BuiltInWebhookEnricher();
        Webhook standardWebhook = getStandardWebhook();
        WebhookPublishRequest request = WebhookPublishRequest.builder(WebhookDiagnosticsEvent.PING, null).build();
        WebhookInvocation invocation = new DefaultWebhookInvocation(standardWebhook, request);

        enricher.enrich(invocation);

        assertThat(invocation.getRequestBuilder().getHeaders(),
                hasEntry("X-Event-Key", WebhookDiagnosticsEvent.PING.getId()));
    }

    private Webhook getStandardWebhook() {
        Webhook webhook = mock(Webhook.class);
        when(webhook.getId()).thenReturn(1);
        when(webhook.getUrl()).thenReturn(WebhookTestUtils.DEFAULT_URL);
        return webhook;
    }

}