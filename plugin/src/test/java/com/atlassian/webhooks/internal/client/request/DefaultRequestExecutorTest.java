package com.atlassian.webhooks.internal.client.request;

import com.atlassian.httpclient.apache.httpcomponents.ApacheAsyncHttpClient;
import com.atlassian.httpclient.api.BannedHostException;
import com.atlassian.httpclient.api.factory.HttpClientFactory;
import com.atlassian.httpclient.api.factory.HttpClientOptions;
import com.atlassian.webhooks.WebhooksConfiguration;
import com.atlassian.webhooks.WebhooksNotInitializedException;
import com.atlassian.webhooks.request.Method;
import com.atlassian.webhooks.request.WebhookHttpResponse;
import com.atlassian.webhooks.request.WebhookResponseBody;
import com.google.common.base.Strings;
import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableList;
import org.apache.commons.io.IOUtils;
import org.apache.http.conn.UnsupportedSchemeException;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static java.util.Optional.of;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;

public class DefaultRequestExecutorTest {

    @Rule
    public WebhooksTestServerRule httpServer = new WebhooksTestServerRule();
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule().silent();
    private DefaultRequestExecutor executor;
    @Mock
    private HttpClientFactory httpClientFactory;
    private InetSocketAddress requestDetails;

    @Before
    public void setup() {
        when(httpClientFactory.create(ArgumentMatchers.any())).thenAnswer(invocation -> {
            HttpClientOptions options = invocation.getArgument(0);
            return new ApacheAsyncHttpClient("test-client", options);
        });
        executor = new DefaultRequestExecutor(httpClientFactory);
        requestDetails = httpServer.getServer().getAddress();
    }

    @After
    public void tearDown() throws Exception {
        if (executor != null) {
            executor.onStop();
        }
    }

    @Test
    public void testAcceptConnection() throws ExecutionException, InterruptedException, IOException {
        executor.onStart(WebhooksConfiguration.DEFAULT);

        RawRequest.Builder builder = RawRequest.builder()
                .header("Test", "value")
                .header("Content-Type", "application/json")
                .method(Method.POST)
                .url("http://localhost:" + requestDetails.getPort() + "/echo");

        builder.asPayloadBuilder().body("{'hi': true}".getBytes(StandardCharsets.UTF_8), "application/json");

        CompletableFuture<WebhookHttpResponse> execute = executor.execute(builder.build());

        WebhookHttpResponse response = execute.get();
        WebhookResponseBody body = response.getBody();
        ByteArrayOutputStream returnBody = new ByteArrayOutputStream();
        IOUtils.copy(response.getBody().getContent(), returnBody);

        assertThat(response.getStatusCode(), is(200));
        assertThat(body.getContentType().isPresent(), is(true));
        assertThat(body.getContentType(), is(of("application/json")));
        assertThat(response.getHeaders().getHeader("Test"), is("value"));
        assertThat(returnBody.toString(), equalTo("{'hi': true}"));
    }

    @Test
    public void testBlacklistConnection() throws ExecutionException, InterruptedException {
        executor.onStart(new WebhooksConfiguration() {
            @Override
            public List<String> getBlacklistedAddresses() {
                return ImmutableList.of("192.168.0.0/16", "8.8.8.8", "fd12:3456:7780:234f::/64");
            }
        });

        ImmutableList<String> testIps = ImmutableList.of("192.168.20.50", "8.8.8.8", "192.168.255.255", "[fd12:3456:7780:234f:0:0:d22f:0]");
        for(String testIp: testIps) {

            CompletableFuture<WebhookHttpResponse> result = executor.execute(
                    RawRequest.builder(Method.POST, "http://" + testIp).build());

            result.handle((x, throwable) -> {
                assertThat(Throwables.getRootCause(throwable), instanceOf(BannedHostException.class));
                return null;
            }).get();
        }
    }

    @Test
    public void testBlacklistConnectionStillAllowsForRegularOperation() throws ExecutionException, InterruptedException {
        executor.onStart(new WebhooksConfiguration() {
            @Override
            public List<String> getBlacklistedAddresses() {
                return ImmutableList.of("192.168.0.1");
            }
        });

        WebhookHttpResponse result = executor.execute(RawRequest.builder(Method.POST,
                "http://localhost:" + requestDetails.getPort() + "/no-content")
                .build()).get();
        assertThat(result.getStatusCode(), is(204));
    }

    @Test
    public void testInvalidURISyntax() throws ExecutionException, InterruptedException {
        executor.onStart(WebhooksConfiguration.DEFAULT);

        CompletableFuture<WebhookHttpResponse> result = executor.execute(
                RawRequest.builder(Method.GET, "://🔥🔥🔥🔥").build());

        assertThat(result.isCompletedExceptionally(), is(true));
        result.handle((x, throwable) -> {
            assertThat(Throwables.getRootCause(throwable), instanceOf(URISyntaxException.class));
            return null;
        }).get();
    }

    @Test
    public void testNonExistentScheme() throws ExecutionException, InterruptedException {
        executor.onStart(WebhooksConfiguration.DEFAULT);

        CompletableFuture<WebhookHttpResponse> result = executor.execute(RawRequest.builder(Method.POST,
                "notarealscheme://localhost:" + requestDetails.getPort() + "/doesntexist")
                .build());

        result.handle((response, throwable) -> {
            assertThat(Throwables.getRootCause(throwable), instanceOf(UnsupportedSchemeException.class));
            return null;
        }).get();
    }

    @Test
    public void testNoContent() throws ExecutionException, InterruptedException, IOException {
        executor.onStart(WebhooksConfiguration.DEFAULT);

        WebhookHttpResponse result = executor.execute(RawRequest.builder(Method.POST,
                "http://localhost:" + requestDetails.getPort() + "/no-content")
                .build()).get();

        assertThat(result.getStatusCode(), is(204));
        WebhookResponseBody body = result.getBody();
        assertThat(body, notNullValue());
        assertThat(body.getContent(), notNullValue());
        byte[] buffer = new byte[1024];
        assertThat(body.getContent().read(buffer), is(-1));
    }

    @Test
    public void testNotFound() throws ExecutionException, InterruptedException {
        executor.onStart(WebhooksConfiguration.DEFAULT);

        WebhookHttpResponse result = executor.execute(RawRequest.builder(Method.POST,
                "http://localhost:" + requestDetails.getPort() + "/doesntexist")
                .build()).get();

        assertThat(result.getStatusCode(), is(404));
    }

    @Test
    public void testOnStartNotCalled() throws ExecutionException, InterruptedException {
        CompletableFuture<WebhookHttpResponse> result = executor.execute(RawRequest.builder(Method.POST,
                "http://localhost:" + requestDetails.getPort() + "/doesntexist")
                .build());

        assertThat(result.isCompletedExceptionally(), is(true));
        result.handle((response, throwable) -> {
            assertThat(throwable, instanceOf(WebhooksNotInitializedException.class));
            return null;
        }).get();
    }

    @Test
    public void testResponseIsTruncatedIfTooLong() throws Exception {
        executor.onStart(new WebhooksConfiguration() {
            @Override
            public long getMaxResponseBodySize() {
                // set the maximum response size to 100
                return 100L;
            }
        });

        RawRequest.Builder builder = RawRequest.builder()
                .header("Test", "value")
                .header("Content-Type", "application/json")
                .method(Method.POST)
                .url("http://localhost:" + requestDetails.getPort() + "/echo");

        // send a body of 500 chars - the echo service will return it unchanged
        builder.asPayloadBuilder().body(Strings.repeat("A", 500).getBytes(StandardCharsets.UTF_8), "application/json");

        CompletableFuture<WebhookHttpResponse> execute = executor.execute(builder.build());
        WebhookHttpResponse response = execute.get();
        WebhookResponseBody body = response.getBody();
        ByteArrayOutputStream returnBody = new ByteArrayOutputStream();
        IOUtils.copy(response.getBody().getContent(), returnBody);

        // validate that the status, headers and first 100 bytes of the body are available
        assertThat(returnBody.size(), is(100));
        assertThat(response.getStatusCode(), is(200));
        assertThat(body.getContentType().isPresent(), is(true));
        assertThat(body.getContentType(), is(of("application/json")));
        assertThat(response.getHeaders().getHeader("Test"), is("value"));
        assertThat(returnBody.toString(), equalTo(Strings.repeat("A", 100)));
    }

    @Test
    public void testUnknownHost() throws Exception {
        executor.onStart(WebhooksConfiguration.DEFAULT);

        CompletableFuture<WebhookHttpResponse> result = executor.execute(RawRequest.builder(Method.POST,
                "http://abcdefg.hijk.lm.no.pqwq.iuwoueqweuqow.com:" + requestDetails.getPort() + "/doesntexist")
                .build());

        result.handle((response, throwable) -> {
            assertThat(Throwables.getRootCause(throwable), instanceOf(UnknownHostException.class));
            return null;
        }).get();
    }
}