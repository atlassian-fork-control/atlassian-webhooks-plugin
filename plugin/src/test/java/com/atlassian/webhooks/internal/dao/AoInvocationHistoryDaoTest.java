package com.atlassian.webhooks.internal.dao;

import com.atlassian.activeobjects.test.TestActiveObjects;
import com.atlassian.webhooks.history.DetailedInvocation;
import com.atlassian.webhooks.history.DetailedInvocationError;
import com.atlassian.webhooks.history.DetailedInvocationRequest;
import com.atlassian.webhooks.history.DetailedInvocationResponse;
import com.atlassian.webhooks.history.DetailedInvocationResult;
import com.atlassian.webhooks.history.InvocationCounts;
import com.atlassian.webhooks.history.InvocationOutcome;
import com.atlassian.webhooks.internal.dao.ao.AoDailyInvocationCounts;
import com.atlassian.webhooks.internal.dao.ao.AoHistoricalInvocation;
import com.atlassian.webhooks.internal.history.SimpleDetailedError;
import com.atlassian.webhooks.internal.history.SimpleDetailedInvocation;
import com.atlassian.webhooks.internal.history.SimpleDetailedRequest;
import com.atlassian.webhooks.internal.history.SimpleDetailedResponse;
import com.atlassian.webhooks.request.Method;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import net.java.ao.EntityManager;
import net.java.ao.test.jdbc.Data;
import net.java.ao.test.jdbc.DatabaseUpdater;
import net.java.ao.test.jdbc.Jdbc;
import net.java.ao.test.junit.ActiveObjectsJUnitRunner;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.time.Clock;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.atlassian.webhooks.diagnostics.WebhookDiagnosticsEvent.PING;
import static com.atlassian.webhooks.history.InvocationOutcome.ERROR;
import static com.atlassian.webhooks.history.InvocationOutcome.FAILURE;
import static com.atlassian.webhooks.history.InvocationOutcome.SUCCESS;
import static com.atlassian.webhooks.internal.dao.AoInvocationHistoryDao.DEFAULT_MAX_STRING_LENGTH;
import static java.time.Instant.ofEpochSecond;
import static org.apache.commons.lang3.StringUtils.substring;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Data(AoInvocationHistoryDaoTest.TestDataUpdater.class)
@RunWith(ActiveObjectsJUnitRunner.class)
@Jdbc(SystemPropertyJdbcConfiguration.class)
public class AoInvocationHistoryDaoTest {

    private static final long DAY_MS = TimeUnit.DAYS.toMillis(1);
    private static final int WEBHOOK_ID = 99;

    private Clock clock;
    private AoInvocationHistoryDao dao;
    private EntityManager entityManager;
    private long todayStartTimestamp;

    @Before
    public void setup() {
        clock = mock(Clock.class);
        todayStartTimestamp = System.currentTimeMillis();
        todayStartTimestamp -= todayStartTimestamp % TimeUnit.DAYS.toMillis(1);

        when(clock.millis()).thenReturn(todayStartTimestamp);
        dao = new AoInvocationHistoryDao(new TestActiveObjectsWithOuterTx(entityManager), clock);
    }

    // counts

    @Test
    public void testGetCountsNoDataReturnsZeros() {
        InvocationCounts counts = dao.getCounts(17, "no:such:webhook", 5);

        assertCounts(counts, 0, 0, 0, 5);
    }

    @Test
    public void testAddAndGetCounts() {
        String repoCreated = "repo:created";
        String repoDeleted = "repo:deleted";

        // no counts should be recorded at all to begin with
        InvocationCounts counts = dao.getCounts(WEBHOOK_ID, repoCreated, 30);
        assertCounts(counts, 0, 0, 0, 30);

        // mock some historic data entries
        dao.addCounts(WEBHOOK_ID, repoCreated, new Date(todayStartTimestamp - DAY_MS * 5), 1, 2, 3);
        dao.addCounts(WEBHOOK_ID, repoCreated, new Date(todayStartTimestamp - DAY_MS * 4), 1, 2, 3);
        dao.addCounts(WEBHOOK_ID, repoDeleted, new Date(todayStartTimestamp - DAY_MS * 4), 21, 22, 23);
        dao.addCounts(127312, repoCreated, new Date(todayStartTimestamp - DAY_MS * 4), 11, 12, 13);

        // verify that the counts are filtered correctly on webhook event and webhook ID
        assertCounts(dao.getCounts(WEBHOOK_ID, repoCreated, 30), 2, 4, 6, 30);
        assertCounts(dao.getCounts(WEBHOOK_ID, repoDeleted, 30), 21, 22, 23, 30);
        assertCounts(dao.getCounts(127312, repoCreated, 30), 11, 12, 13, 30);

        // verify that the counts are added correctly if no event is provided
        assertCounts(dao.getCounts(WEBHOOK_ID, null, 30), 23, 26, 29, 30);

        // verify that counts are filtered by date
        assertCounts(dao.getCounts(WEBHOOK_ID, repoCreated, 4), 1, 2, 3, 4);
        assertCounts(dao.getCounts(WEBHOOK_ID, null, 4), 22, 24, 26, 4);

        // verify that multiple adds for the same day work fine too
        dao.addCounts(WEBHOOK_ID, repoCreated, new Date(todayStartTimestamp - DAY_MS), 2, 3, 0);
        dao.addCounts(WEBHOOK_ID, repoCreated, new Date(todayStartTimestamp - DAY_MS), 0, 4, 11);
        assertCounts(dao.getCounts(WEBHOOK_ID, null, 3), 2, 7, 11, 3);
    }

    @Test
    public void testCountsByEventId() {
        String repoCreated = "repo:created";
        String repoDeleted = "repo:deleted";
        String projectCreated = "project:created";
        String other = "other:event";

        // mock some historic data entries
        dao.addCounts(WEBHOOK_ID, repoCreated, new Date(todayStartTimestamp - DAY_MS * 5), 1, 2, 3);
        dao.addCounts(WEBHOOK_ID, repoCreated, new Date(todayStartTimestamp - DAY_MS * 4), 1, 2, 3);
        dao.addCounts(WEBHOOK_ID, repoDeleted, new Date(todayStartTimestamp - DAY_MS * 4), 21, 22, 23);
        dao.addCounts(WEBHOOK_ID, repoCreated, new Date(todayStartTimestamp - DAY_MS * 3), 1, 1, 1);
        dao.addCounts(WEBHOOK_ID, projectCreated, new Date(todayStartTimestamp - DAY_MS * 3), 4, 7, 13);
        dao.addCounts(127312, repoCreated, new Date(todayStartTimestamp - DAY_MS * 4), 11, 12, 13);

        // verify that the counts are filtered correctly on webhook event and webhook ID
        Map<String, InvocationCounts> counts = dao.getCountsByEvent(
                WEBHOOK_ID, ImmutableSet.of(projectCreated, repoCreated, repoDeleted, other), 4);
        assertThat(counts.size(), equalTo(4));
        assertCounts(counts.get(repoCreated), 2, 3, 4, 4);
        assertCounts(counts.get(repoDeleted), 21, 22, 23, 4);
        assertCounts(counts.get(projectCreated), 4, 7, 13, 4);
        assertCounts(counts.get(other), 0, 0, 0, 4);
    }

    @Test
    public void testCountsByWebhookId() {
        String repoCreated = "repo:created";
        String repoDeleted = "repo:deleted";
        String projectCreated = "project:created";
        String other = "other:event";

        // mock some historic data entries
        dao.addCounts(WEBHOOK_ID, repoCreated, new Date(todayStartTimestamp - DAY_MS * 5), 1, 2, 3);
        dao.addCounts(WEBHOOK_ID, repoCreated, new Date(todayStartTimestamp - DAY_MS * 4), 1, 2, 3);
        dao.addCounts(WEBHOOK_ID, repoDeleted, new Date(todayStartTimestamp - DAY_MS * 4), 21, 22, 23);
        dao.addCounts(WEBHOOK_ID, repoCreated, new Date(todayStartTimestamp - DAY_MS * 3), 1, 1, 1);
        dao.addCounts(WEBHOOK_ID, projectCreated, new Date(todayStartTimestamp - DAY_MS * 3), 4, 7, 13);
        dao.addCounts(127312, repoCreated, new Date(todayStartTimestamp - DAY_MS * 4), 11, 12, 13);
        dao.addCounts(11, repoCreated, new Date(todayStartTimestamp - DAY_MS * 4), 10, 11, 12);

        // verify that the counts are mapped correctly by webhook ID and date
        Map<Integer, InvocationCounts> counts = dao.getCountsByWebhook(ImmutableSet.of(WEBHOOK_ID, 11), 4);
        assertThat(counts.size(), equalTo(2));
        assertCounts(counts.get(WEBHOOK_ID), 27, 32, 40, 4);
        assertCounts(counts.get(11), 10, 11, 12, 4);
    }

    @Test
    public void testDeleteCounts() {
        String repoCreated = "repo:created";

        // insert some historic data entries
        dao.addCounts(WEBHOOK_ID, repoCreated, new Date(todayStartTimestamp - DAY_MS * 5), 1, 2, 3);
        dao.addCounts(WEBHOOK_ID, repoCreated, new Date(todayStartTimestamp - DAY_MS * 4), 2, 3, 4);
        dao.addCounts(WEBHOOK_ID, repoCreated, new Date(todayStartTimestamp - DAY_MS * 3), 3, 4, 5);
        dao.addCounts(WEBHOOK_ID, repoCreated, new Date(todayStartTimestamp - DAY_MS * 2), 4, 5, 6);

        // verify the test start state
        assertCounts(dao.getCounts(WEBHOOK_ID, repoCreated, 5), 10, 14, 18, 5);

        dao.deleteDailyCountsOlderThan(3);

        // verify that only the data from 2 and 3 days ago remains
        assertCounts(dao.getCounts(WEBHOOK_ID, repoCreated, 5), 7, 9, 11, 5);
    }

    // AoHistoricalInvocation tests

    @Test
    public void testGetLatestInvocationNotFound() {
        assertThat(dao.getLatestInvocation(99, "no:such:event", null), nullValue());
    }

    @Test
    public void testGetLatestInvocation() {
        dao.saveInvocation(101, createErrorInvocation(1001));
        dao.saveInvocation(101, createFailureInvocation(1002));
        dao.saveInvocation(101, createSuccessInvocation(1003));

        // test retrieving the latest for a single outcome
        AoHistoricalInvocation success = dao.getLatestInvocation(101, PING.getId(), Collections.singleton(ERROR));
        assertThat(success, notNullValue());
        assertThat(success.getOutcome(), equalTo(ERROR));

        // test retrieving the latest across multiple outcomes
        AoHistoricalInvocation retrieved = dao.getLatestInvocation(101, PING.getId(), ImmutableList.of(FAILURE, ERROR));
        assertThat(retrieved, notNullValue());
        assertThat(retrieved.getOutcome(), equalTo(FAILURE));

        // test retrieving the latest across multiple outcomes
        retrieved = dao.getLatestInvocation(101, PING.getId(), ImmutableList.of(FAILURE, ERROR, SUCCESS));
        assertThat(retrieved, notNullValue());
        assertThat(retrieved.getOutcome(), equalTo(SUCCESS));

        // test retrieving the latest across all outcomes through null
        retrieved = dao.getLatestInvocation(101, PING.getId(), null);
        assertThat(retrieved, notNullValue());
        assertThat(retrieved.getOutcome(), equalTo(SUCCESS));
    }

    @Test
    public void testSaveDoesNotReplaceNewerInvocation() {
        SimpleDetailedRequest request = new SimpleDetailedRequest("rbody", Collections.emptyMap(), Method.POST, "url");
        SimpleDetailedResponse response = new SimpleDetailedResponse("result", "desc", Collections.emptyMap(),
                SUCCESS, 200);
        SimpleDetailedInvocation newer = new SimpleDetailedInvocation("newer", PING, request, response,
                ofEpochSecond(1000), ofEpochSecond(1001));
        SimpleDetailedInvocation older = new SimpleDetailedInvocation("older", PING, request, response,
                ofEpochSecond(900), ofEpochSecond(901));

        // store the newer invocation, followed by the older
        dao.saveInvocation(100, newer);
        dao.saveInvocation(100, older);

        // verify that the newer invocation survived
        AoHistoricalInvocation retrieved = dao.getLatestInvocation(100, null, null);
        assertInvocation(newer, 100, retrieved);
    }

    @Test
    public void testSaveError() {
        SimpleDetailedInvocation invocation = createErrorInvocation(1873794832);
        dao.saveInvocation(99, invocation);

        AoHistoricalInvocation retrieved = dao.getLatestInvocation(99, null, null);
        assertInvocation(invocation, 99, retrieved);
    }

    @Test
    public void testSaveFailure() {
        SimpleDetailedInvocation invocation = createFailureInvocation(10001);
        dao.saveInvocation(99, invocation);

        AoHistoricalInvocation retrieved = dao.getLatestInvocation(99, null, null);
        assertInvocation(invocation, 99, retrieved);
    }

    @Test
    public void testSaveRequestWithLongFields() {
        String longUrl = "http://" + StringUtils.repeat("x", DEFAULT_MAX_STRING_LENGTH);
        SimpleDetailedRequest request = new SimpleDetailedRequest("request-body",
                ImmutableMap.of("Header-1", "Value-1", "Header-2", "Value-2"), Method.GET, longUrl);

        String longDescription = "java.lang.RuntimeException: java.net.UnknownHostException: " + longUrl;
        SimpleDetailedResponse success = new SimpleDetailedResponse("response-body", longDescription,
                ImmutableMap.of("Header-1", "resp-1", "Header-2", "resp2"), SUCCESS, 200);
        SimpleDetailedInvocation invocation =  new SimpleDetailedInvocation("invocation-id", PING, request, success,
                ofEpochSecond(1001), ofEpochSecond(1001 + 2));
        dao.saveInvocation(99, invocation);

        AoHistoricalInvocation retrieved = dao.getLatestInvocation(99, null, null);
        assertThat(retrieved, notNullValue());
        assertThat(substring(longUrl, 0, DEFAULT_MAX_STRING_LENGTH), equalTo(retrieved.getRequestUrl()));
        assertThat(substring(longDescription, 0, DEFAULT_MAX_STRING_LENGTH),
                equalTo(retrieved.getResultDescription()));
    }

    @Test
    public void testSaveSuccess() {
        SimpleDetailedInvocation invocation = createSuccessInvocation(1001);
        dao.saveInvocation(99, invocation);

        AoHistoricalInvocation retrieved = dao.getLatestInvocation(99, null, null);
        assertInvocation(invocation, 99, retrieved);
    }

    // other DAO methods

    @Test
    public void testDeleteForWebhook() {
        // set up some test data
        dao.addCounts(WEBHOOK_ID, "repo:created", new Date(todayStartTimestamp - DAY_MS * 5), 1, 2, 3);
        dao.addCounts(1234, "repo:created", new Date(todayStartTimestamp - DAY_MS * 5), 4, 5, 6);

        dao.deleteForWebhook(WEBHOOK_ID);

        // verify that the daily counts have been deleted for the webhook, but not for others
        assertCounts(dao.getCounts(WEBHOOK_ID, null, 30), 0, 0, 0, 30);
        assertCounts(dao.getCounts(1234, null, 30), 4, 5, 6, 30);
    }

    private void assertCounts(InvocationCounts counts, int errors, int failures, int successes, int days) {
        assertThat(counts, notNullValue());
        assertThat(counts.getErrors(), equalTo(errors));
        assertThat(counts.getFailures(), equalTo(failures));
        assertThat(counts.getSuccesses(), equalTo(successes));
        assertThat(counts.getWindowDuration(), equalTo(Duration.of(days, ChronoUnit.DAYS)));
    }

    private void assertInvocation(DetailedInvocation expected, int webhookId, AoHistoricalInvocation actual) {
        assertThat(actual, notNullValue());
        DetailedInvocationResult result = expected.getResult();
        DetailedInvocationRequest request = expected.getRequest();
        assertThat(actual.getEventId(), equalTo(expected.getEvent().getId()));
        assertThat(actual.getFinish(), equalTo(expected.getFinish().toEpochMilli()));
        assertThat(actual.getOutcome(), equalTo(result.getOutcome()));
        assertThat(actual.getRequestBody(), equalTo(request.getBody().orElse(null)));
        assertThat(actual.getRequestMethod(), equalTo(request.getMethod().name()));
        assertThat(actual.getRequestUrl(), equalTo(request.getUrl()));
        assertThat(actual.getRequestId(), equalTo(expected.getId()));
        assertThat(actual.getResultDescription(), equalTo(result.getDescription()));
        assertThat(actual.getStart(), equalTo(expected.getStart().toEpochMilli()));
        assertThat(actual.getWebhookId(), equalTo(webhookId));
        Map<String, String> reqHeaders = dao.decodeHeaders("id", actual.getRequestHeaders());
        assertThat(reqHeaders, equalTo(expected.getRequest().getHeaders()));

        if (result instanceof DetailedInvocationError) {
            DetailedInvocationError error = (DetailedInvocationError) result;
            assertThat(actual.getErrorContent(), equalTo(error.getContent()));
            assertThat(actual.getStatusCode(), nullValue());
            assertThat(actual.getResponseBody(), nullValue());
            assertThat(actual.getResponseHeaders(), nullValue());
        } else  if (result instanceof DetailedInvocationResponse) {
            DetailedInvocationResponse response = (DetailedInvocationResponse) result;
            assertThat(actual.getErrorContent(), nullValue());
            assertThat(actual.getStatusCode(), equalTo(response.getStatusCode()));
            assertThat(actual.getResponseBody(), equalTo(response.getBody().orElse(null)));
            Map<String, String> respHeaders = dao.decodeHeaders("id", actual.getResponseHeaders());
            assertThat(respHeaders, equalTo(response.getHeaders()));
        }
    }

    private SimpleDetailedInvocation createErrorInvocation(long epochSeconds) {
        SimpleDetailedRequest request = new SimpleDetailedRequest(
                "request-body", ImmutableMap.of("Header-1", "Value-1", "Header-2", "Value-2"),
                Method.GET, "http://url.com");
        SimpleDetailedError error = new SimpleDetailedError("error-content", "error-description");
        return new SimpleDetailedInvocation("invocation-id",
                PING, request, error,
                ofEpochSecond(epochSeconds), ofEpochSecond(epochSeconds + 2));
    }

    private SimpleDetailedInvocation createFailureInvocation(long epochSeconds) {
        SimpleDetailedRequest request = new SimpleDetailedRequest(
                "request-body", ImmutableMap.of("Header-1", "Value-1", "Header-2", "Value-2"),
                Method.GET, "http://url.com");
        SimpleDetailedResponse failure = new SimpleDetailedResponse("response-body", "response-description",
                ImmutableMap.of("Header-1", "resp-1", "Header-2", "resp2"), InvocationOutcome.FAILURE,
                500);
        return new SimpleDetailedInvocation("invocation-id",
                PING, request, failure,
                ofEpochSecond(epochSeconds), ofEpochSecond(epochSeconds + 2));
    }

    private SimpleDetailedInvocation createSuccessInvocation(long epochSeconds) {
        SimpleDetailedRequest request = new SimpleDetailedRequest(
                "request-body", ImmutableMap.of("Header-1", "Value-1", "Header-2", "Value-2"),
                Method.GET, "http://url.com");
        SimpleDetailedResponse success = new SimpleDetailedResponse("response-body", "response-description",
                ImmutableMap.of("Header-1", "resp-1", "Header-2", "resp2"), SUCCESS,
                200);
        return new SimpleDetailedInvocation("invocation-id",
                PING, request, success,
                ofEpochSecond(epochSeconds), ofEpochSecond(epochSeconds + 2));
    }

    public static class TestDataUpdater implements DatabaseUpdater {

        @Override
        public void update(EntityManager entityManager) throws Exception {
            //noinspection unchecked
            entityManager.migrate(AoHistoricalInvocation.class, AoDailyInvocationCounts.class);
        }
    }
}