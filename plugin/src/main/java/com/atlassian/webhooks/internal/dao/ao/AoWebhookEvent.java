package com.atlassian.webhooks.internal.dao.ao;

import net.java.ao.Accessor;
import net.java.ao.Entity;
import net.java.ao.Mutator;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.Table;

import javax.annotation.Nonnull;

@Table(AoWebhookEvent.TABLE_NAME)
public interface AoWebhookEvent extends Entity {

    String EVENT_ID_COLUMN = "EVENT_ID";
    String TABLE_NAME = "WEBHOOK_EVENT";
    String WEBHOOK_COLUMN = "WEBHOOK";

    // This is used to allow foreign key style queries in AO.
    // It's standard for AO to just add 'ID' to the end of the column
    // specified by the Mutator/Accessor
    String WEBHOOK_COLUMN_QUERY = WEBHOOK_COLUMN + "ID";

    @NotNull
    @Accessor(EVENT_ID_COLUMN)
    String getEventId();

    @NotNull
    @Accessor(WEBHOOK_COLUMN)
    AoWebhook getWebhook();

    @Mutator(EVENT_ID_COLUMN)
    void setEventId(@Nonnull String value);

    @Mutator(WEBHOOK_COLUMN)
    void setWebhook(@Nonnull AoWebhook webhook);
}
