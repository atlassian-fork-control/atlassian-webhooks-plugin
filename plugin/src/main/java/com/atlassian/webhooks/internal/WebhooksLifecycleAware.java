package com.atlassian.webhooks.internal;

import com.atlassian.webhooks.WebhooksConfiguration;

public interface WebhooksLifecycleAware {

    void onStart(WebhooksConfiguration configuration);

    default void onStop() {
    }
}
