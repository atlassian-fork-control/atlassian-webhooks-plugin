package com.atlassian.webhooks.internal.jmx;

public interface WebhooksMXBean {

    long getDispatchCount();

    long getDispatchErrorCount();

    long getDispatchFailureCount();

    long getDispatchInFlightCount();

    long getDispatchLastRejectedTimestamp();

    long getDispatchRejectedCount();

    long getDispatchSuccessCount();

    long getPublishCount();
}
