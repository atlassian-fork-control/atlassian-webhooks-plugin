package com.atlassian.webhooks.internal.publish;

import com.atlassian.webhooks.DispatchFailedException;
import com.atlassian.webhooks.WebhookCallback;
import com.atlassian.webhooks.WebhooksConfiguration;
import com.atlassian.webhooks.WebhooksNotInitializedException;
import com.atlassian.webhooks.diagnostics.WebhookDiagnosticsEvent;
import com.atlassian.webhooks.internal.WebhooksLifecycleAware;
import com.atlassian.webhooks.internal.client.RequestExecutor;
import com.atlassian.webhooks.request.WebhookHttpRequest;
import com.atlassian.webhooks.request.WebhookHttpResponse;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Throwables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.time.Clock;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

/**
 * Dispatcher implementation that restricts the total number of in flight dispatches to avoid out of memory
 * issues when too many webhook dispatches take too long. This is necessary because Apache's async http-client (which
 * is used internally by requestExecutor) has an unbounded request queue. If too many webhook listeners are slow to
 * respond, we can end up hitting memory limits when there's a peak in webhook triggers.
 * <p>
 * This implementation also backs off on dispatching of webhook events to listeners that have failed too often in a row,
 * retrying at exponentially increasing intervals.
 */
@Component
public class DefaultWebhookDispatcher implements WebhookDispatcher, WebhooksLifecycleAware {

    private static final Logger log = LoggerFactory.getLogger(DefaultWebhookDispatcher.class);

    private final ConcurrentMap<Integer, WebhookCircuitBreaker> circuitBreakers;
    private final Clock clock;
    private final RequestExecutor requestExecutor;

    private Semaphore dispatchTickets;
    private long ticketTimeoutMillis;

    private volatile WebhooksConfiguration config;
    private volatile long lastRejectedTimestamp;

    DefaultWebhookDispatcher(Clock clock, RequestExecutor requestExecutor) {
        this.clock = clock;
        this.requestExecutor = requestExecutor;

        circuitBreakers = new ConcurrentHashMap<>();
        config = WebhooksConfiguration.DEFAULT;
    }

    @Autowired
    public DefaultWebhookDispatcher(RequestExecutor requestExecutor) {
        this(Clock.systemDefaultZone(), requestExecutor);
    }

    @Override
    public void dispatch(@Nonnull InternalWebhookInvocation invocation) {
        log.debug("Starting dispatch work for webhook invocation [{}]", invocation.getId());

        WebhookHttpRequest request = invocation.getRequestBuilder().build();
        Ticket ticket = acquireTicket(invocation);

        if (ticket == null) {
            onSkipped(invocation, request, "Too many webhook dispatches already in flight");
            return;
        }
        WebhookCircuitBreaker circuitBreaker = circuitBreakers.get(invocation.getWebhook().getId());
        if (circuitBreaker != null) {
            long delay = circuitBreaker.getMillisToNextAttempt();
            if (delay > 0) {
                // Here we'd managed to get a ticket, but we've just found out we're not allowed one, lets hand it back
                ticket.close();
                onSkipped(invocation, request,
                        "Webhook failed too many times. Skipping this webhook for the next " + delay + "ms");
                return;
            }
        }

        try {
            requestExecutor.execute(request)
                    .handleAsync((webhookHttpResponse, throwable) -> {
                        try {
                            int statusCode = throwable == null ? webhookHttpResponse.getStatusCode() : -1;
                            log.debug("Request has completed for webhook invocation [{}]. Status code = {}",
                                    invocation.getId(), statusCode);
                            if (throwable != null) {
                                if (throwable instanceof WebhooksNotInitializedException) {
                                    onSkipped(invocation, request, throwable.getLocalizedMessage());
                                } else {
                                    onError(invocation, request, throwable);
                                }
                            } else if (statusCode >= 200 && statusCode < 300) {
                                onSuccess(invocation, request, webhookHttpResponse);
                            } else {
                                onFailure(invocation, request, webhookHttpResponse);
                            }
                        } finally {
                            // release the ticket _after_ the callbacks have completed
                            ticket.close();
                        }
                        return null;
                    });
        } catch (Throwable t) {
            try {
                onError(invocation, request, t);
            } finally {
                // make sure the ticket is released
                ticket.close();
            }
            throw Throwables.propagate(t);
        }
    }

    @Override
    public int getInFlightCount() {
        if(dispatchTickets == null) return -1;

        return config.getMaxInFlightDispatches() - dispatchTickets.availablePermits();
    }

    @Override
    public long getLastRejectedTimestamp() {
        return lastRejectedTimestamp;
    }

    @Override
    public void onStart(WebhooksConfiguration configuration) {
        config = configuration;
        dispatchTickets = new Semaphore(configuration.getMaxInFlightDispatches());
        ticketTimeoutMillis = configuration.getDispatchTimeout().toMillis();
    }

    @Override
    public void onStop() {
        config = WebhooksConfiguration.DEFAULT;
    }

    @VisibleForTesting
    int getAvailableTickets() {
        return dispatchTickets.availablePermits();
    }

    private static Consumer<WebhookCallback> safely(Consumer<WebhookCallback> consumer) {
        return callback -> {
            try {
                consumer.accept(callback);
            } catch (RuntimeException e) {
                log.warn("Webhook callback failed", e);
            }
        };
    }

    private Ticket acquireTicket(InternalWebhookInvocation invocation) {
        try {
            if(dispatchTickets == null) {
                log.warn("A ticket was acquired before the webhooks plugin was started, this dispatch will be allowed");
                return new DummyTicket();
            }

            if (!dispatchTickets.tryAcquire(ticketTimeoutMillis, TimeUnit.MILLISECONDS)) {
                log.warn("Could not dispatch {} webhook to {}; a maximum of {} dispatches are already in flight",
                        invocation.getEvent().getId(), invocation.getWebhook().getUrl(),
                        config.getMaxInFlightDispatches());
                return null;
            }
            return new DispatchTicket();
        } catch (InterruptedException e) {
            // restore interrupted flag
            Thread.currentThread().interrupt();
        }
        return null;
    }

    private long calculateNextAttemptTimestamp(int failureCount) {
        int significantFailures = failureCount - config.getBackoffTriggerCount();
        if (significantFailures >= 0) {
            return clock.millis() + Math.min(config.getBackoffMaxDelay().toMillis(),
                            Math.round(config.getBackoffInitialDelay().toMillis() *
                                    Math.pow(config.getBackoffExponent(), significantFailures)));
        }
        // anything lower than the current time will do if the webhook hasn't failed often enough
        return 0;
    }

    private void onError(InternalWebhookInvocation invocation, WebhookHttpRequest request, Throwable throwable) {
        log.info("Webhook invocation [{}] to [{}] failed with an error", invocation.getId(),
                invocation.getWebhook().getUrl(), log.isDebugEnabled() ? throwable : null);

        updateCircuitBreakerWithFailure(invocation);
        invocation.getCallbacks().forEach(safely(callback -> {
            if (log.isTraceEnabled()) {
                log.trace("Call back to [{}] from webhook invocation [{}] in error",
                        callback.getClass().getSimpleName(), invocation.getId());
            }
            callback.onError(request, throwable, invocation);
        }));
    }

    private void onFailure(InternalWebhookInvocation invocation, WebhookHttpRequest request, WebhookHttpResponse response) {
        updateCircuitBreakerWithFailure(invocation);
        invocation.getCallbacks().forEach(safely(callback -> {
            if (log.isTraceEnabled()) {
                log.trace("Call back to [{}] from failed webhook invocation [{}]",
                        callback.getClass().getSimpleName(), invocation.getId());
            }
            callback.onFailure(request, response, invocation);
        }));
    }

    private void onSkipped(InternalWebhookInvocation invocation, WebhookHttpRequest request, String message) {
        lastRejectedTimestamp = clock.millis();
        DispatchFailedException exception = new DispatchFailedException(invocation, message);
        log.debug("Skipping webhook invocation [{}] to {} ({})", invocation.getId(), invocation.getWebhook().getUrl(),
                message);
        invocation.getCallbacks().forEach(safely(callback -> callback.onError(request, exception, invocation)));
    }

    private void onSuccess(InternalWebhookInvocation invocation, WebhookHttpRequest request, WebhookHttpResponse response) {
        circuitBreakers.remove(invocation.getWebhook().getId());
        invocation.getCallbacks().forEach(safely(callback -> {
            if (log.isTraceEnabled()) {
                log.trace("Call back to [{}] from successful webhook invocation [{}]",
                        callback.getClass().getSimpleName(), invocation.getId());
            }
            callback.onSuccess(request, response, invocation);
        }));
    }

    private void updateCircuitBreakerWithFailure(InternalWebhookInvocation invocation) {
        if (!(invocation.getEvent() instanceof WebhookDiagnosticsEvent)) {
            // don't circuit break on failures for diagnostic events
            circuitBreakers.computeIfAbsent(invocation.getWebhook().getId(), id -> new WebhookCircuitBreaker())
                    .onFailure();
        }
    }

    private class WebhookCircuitBreaker {

        private int failureCount;
        private long nextAttemptTimestamp;

        synchronized long getMillisToNextAttempt() {
            return Math.max(0, nextAttemptTimestamp - clock.millis());
        }

        synchronized void onFailure() {
            nextAttemptTimestamp = calculateNextAttemptTimestamp(++failureCount);
        }
    }

    private interface Ticket extends AutoCloseable {
        @Override
        void close();
    }

    private class DispatchTicket implements Ticket {
        @Override
        public void close(){
            dispatchTickets.release();
        }
    }

    private static class DummyTicket implements Ticket {
        @Override
        public void close(){
        }
    }
}
