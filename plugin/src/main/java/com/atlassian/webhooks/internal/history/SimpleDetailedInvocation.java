package com.atlassian.webhooks.internal.history;

import com.atlassian.webhooks.WebhookEvent;
import com.atlassian.webhooks.WebhookInvocation;
import com.atlassian.webhooks.history.*;
import com.atlassian.webhooks.request.WebhookHttpRequest;
import com.atlassian.webhooks.request.WebhookHttpResponse;

import javax.annotation.Nonnull;
import java.time.Instant;

public class SimpleDetailedInvocation extends SimpleHistoricalInvocation implements DetailedInvocation {

    public SimpleDetailedInvocation(@Nonnull String id, @Nonnull WebhookEvent event, @Nonnull InvocationRequest request,
                                    @Nonnull InvocationResult result, @Nonnull Instant start, @Nonnull Instant finish) {
        super(id, event, start, finish, request, result);
    }

    public SimpleDetailedInvocation(@Nonnull WebhookInvocation invocation,
                                    @Nonnull WebhookHttpRequest request, @Nonnull Throwable error,
                                    @Nonnull Instant start, @Nonnull Instant finish) {

        super(invocation.getId(), invocation.getEvent(), start, finish, new SimpleDetailedRequest(invocation, request),
                new SimpleDetailedError(error));
    }

    public SimpleDetailedInvocation(@Nonnull WebhookInvocation invocation, @Nonnull InvocationOutcome resultKind,
                                    @Nonnull WebhookHttpRequest request, @Nonnull WebhookHttpResponse response,
                                    @Nonnull Instant start, @Nonnull Instant finish) {
        super(invocation.getId(), invocation.getEvent(), start, finish, new SimpleDetailedRequest(invocation, request),
                new SimpleDetailedResponse(resultKind, invocation, response));
    }

    @Nonnull
    @Override
    public DetailedInvocationRequest getRequest() {
        return (DetailedInvocationRequest) super.getRequest();
    }

    @Nonnull
    @Override
    public DetailedInvocationResult getResult() {
        return (DetailedInvocationResult) super.getResult();
    }
}
