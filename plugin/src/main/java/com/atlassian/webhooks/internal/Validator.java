package com.atlassian.webhooks.internal;

/**
 * Validator that performs JSR-303 validations if a JSR-303 Validator has been made available over OSGI. If no
 * validator is provided, validation will not be performed.
 */
public interface Validator {

    <T> T validate(T object);
}
