package com.atlassian.webhooks.internal.jmx;

import com.atlassian.webhooks.WebhookService;
import com.atlassian.webhooks.WebhooksConfiguration;
import com.atlassian.webhooks.internal.WebhooksLifecycleAware;
import com.atlassian.webhooks.internal.publish.WebhookDispatcher;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.management.*;
import java.lang.management.ManagementFactory;

@Component
public class JmxBootstrap implements WebhooksLifecycleAware {

    private static final Logger log = LoggerFactory.getLogger(JmxBootstrap.class);

    private final WebhookDispatcher dispatcher;
    private final WebhookService webhookService;

    private volatile ObjectName mbeanName;

    @Autowired
    public JmxBootstrap(WebhookDispatcher dispatcher, WebhookService webhookService) {
        this.dispatcher = dispatcher;
        this.webhookService = webhookService;
    }

    public void onStart(WebhooksConfiguration configuration) {
        try {
            mbeanName = new ObjectName(configuration.getJmxDomain() + ":name=Webhooks");
        } catch (MalformedObjectNameException e) {
            log.warn("Could not determine webhooks MBean name", e);
        }

        MBeanServer server = getMBeanServer();
        if (server != null && mbeanName != null) {
            try {
                server.registerMBean(new WebhooksMXBeanAdapter(dispatcher, webhookService), mbeanName);
            } catch (InstanceAlreadyExistsException | MBeanRegistrationException | NotCompliantMBeanException e) {
                log.warn("Failed to register MBean {}", mbeanName, e);
            }
        }
    }

    public void onStop() {
        MBeanServer server = getMBeanServer();
        ObjectName name = mbeanName;
        if (server != null && name != null) {
            try {
                server.unregisterMBean(mbeanName);
            } catch (InstanceNotFoundException e) {
                // that's ok, no need to complain
            } catch (MBeanRegistrationException e) {
                log.info("Failed to unregister MBean {}", mbeanName, e);
            }
        }
        mbeanName = null;
    }

    @VisibleForTesting
    MBeanServer getMBeanServer() {
        return ManagementFactory.getPlatformMBeanServer();
    }
}
