package com.atlassian.webhooks.internal.client.request;

import com.atlassian.httpclient.api.HttpClient;
import com.atlassian.httpclient.api.Request;
import com.atlassian.httpclient.api.Response;
import com.atlassian.httpclient.api.ResponseTooLargeException;
import com.atlassian.httpclient.api.factory.HttpClientFactory;
import com.atlassian.httpclient.api.factory.HttpClientOptions;
import com.atlassian.webhooks.WebhooksConfiguration;
import com.atlassian.webhooks.WebhooksNotInitializedException;
import com.atlassian.webhooks.internal.WebhooksLifecycleAware;
import com.atlassian.webhooks.internal.client.RequestExecutor;
import com.atlassian.webhooks.request.WebhookHttpRequest;
import com.atlassian.webhooks.request.WebhookHttpResponse;
import com.google.common.base.MoreObjects;
import com.google.common.primitives.Ints;
import io.atlassian.util.concurrent.Promise;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.io.ByteArrayInputStream;
import java.net.URI;
import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

@Component("requestExecutor")
public class DefaultRequestExecutor implements RequestExecutor, WebhooksLifecycleAware {

    private static final Logger log = LoggerFactory.getLogger(DefaultRequestExecutor.class);

    private final HttpClientFactory httpClientFactory;

    private volatile HttpClient client;
    private volatile WebhooksConfiguration configuration;

    @Autowired
    public DefaultRequestExecutor(HttpClientFactory httpClientFactory) {
        this.httpClientFactory = httpClientFactory;
    }

    @Nonnull
    @Override
    public CompletableFuture<WebhookHttpResponse> execute(@Nonnull WebhookHttpRequest request) {
        CompletableFuture<WebhookHttpResponse> responseFuture = new CompletableFuture<>();
        try {
            HttpClient httpClient = client;
            if (httpClient == null) {
                throw new WebhooksNotInitializedException("The webhooks plugin hasn't been initialized yet. " +
                        "Webhook will not be dispatched.");
            }
            URI uri = URI.create(request.getUrl());
            Request.Builder builder = httpClient.newRequest()
                    .setUri(uri);

            request.getHeaders().forEach(builder::setHeader);

            if (request.getContent() != null) {
                builder
                        .setEntityStream(new ByteArrayInputStream(request.getContent()))
                        .setContentType(request.getContentType().orElseThrow(() ->
                                new IllegalStateException("If content is provided, Content-Type must also be specified")));
            }

            builder.execute(Request.Method.valueOf(request.getMethod().name()))
                    .then(new Promise.TryConsumer<Response>() {
                        @Override
                        public void accept(@Nonnull Response response) {
                            responseFuture.complete(transform(response));
                        }

                        @Override
                        public void fail(@Nonnull Throwable throwable) {
                            if (throwable instanceof ResponseTooLargeException) {
                                // the partial response contains the headers, status and the first maxEntitySize bytes,
                                // which is enough for webhooks to count it as a 'normal' response
                                ResponseTooLargeException tooLarge = (ResponseTooLargeException) throwable;
                                accept(tooLarge.getResponse());
                            } else {
                                responseFuture.completeExceptionally(throwable);
                            }
                        }
                    });

        } catch (Exception e) {
            responseFuture.completeExceptionally(e);
        }

        return responseFuture;
    }

    @Override
    public void onStart(WebhooksConfiguration configuration) {
        this.configuration = configuration;

        HttpClientOptions options = new HttpClientOptions();
        options.setMaxCacheEntries(0);
        options.setIgnoreCookies(true);
        options.setMaxCallbackThreadPoolSize(configuration.getMaxCallbackThreads());
        options.setMaxConnectionsPerHost(configuration.getMaxHttpConnectionsPerHost());
        options.setMaxEntitySize(configuration.getMaxResponseBodySize());
        options.setConnectionPoolTimeToLive(1, TimeUnit.MINUTES);
        options.setMaxTotalConnections(configuration.getMaxHttpConnections());
        options.setConnectionTimeout(toSeconds(configuration.getConnectionTimeout()), TimeUnit.SECONDS);
        options.setSocketTimeout(toSeconds(configuration.getSocketTimeout()), TimeUnit.SECONDS);
        options.setIoThreadCount(configuration.getIoThreadCount());
        options.setBlacklistedAddresses(configuration.getBlacklistedAddresses());

        client = httpClientFactory.create(options);
    }

    @Override
    public void onStop() {
        HttpClient httpClient = client;
        client = null;
        configuration = null;
        if (httpClient != null) {
            try {
                httpClientFactory.dispose(httpClient);
            } catch (Exception e) {
                log.warn("Error while disposing webhooks HTTP client", e);
            }
        }
    }

    private static int toSeconds(Duration duration) {
        return Ints.saturatedCast(duration.getSeconds());
    }

    private WebhookHttpResponse transform(Response response) {
        return new DefaultRawResponse(response,
                MoreObjects.firstNonNull(configuration, WebhooksConfiguration.DEFAULT).getMaxResponseBodySize());
    }

}
