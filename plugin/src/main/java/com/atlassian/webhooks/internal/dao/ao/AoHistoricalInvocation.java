package com.atlassian.webhooks.internal.dao.ao;

import com.atlassian.webhooks.history.InvocationOutcome;
import net.java.ao.Accessor;
import net.java.ao.Preload;
import net.java.ao.RawEntity;
import net.java.ao.Transient;
import net.java.ao.schema.Index;
import net.java.ao.schema.Indexes;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.PrimaryKey;
import net.java.ao.schema.StringLength;
import net.java.ao.schema.Table;

import static net.java.ao.schema.StringLength.UNLIMITED;

@Preload({
        AoHistoricalInvocation.COLUMN_FINISH,
        AoHistoricalInvocation.COLUMN_WEBHOOK_ID,
})
@Indexes({
        @Index(name = AoHistoricalInvocation.INDEX_WEBHOOK_EVENT_RESULT, methodNames = {
                AoHistoricalInvocation.COLUMN_WEBHOOK_ID,
                AoHistoricalInvocation.COLUMN_EVENT_ID,
                AoHistoricalInvocation.COLUMN_OUTCOME
        }),
        @Index(name = AoHistoricalInvocation.INDEX_WEBHOOK_RESULT, methodNames = {
                AoHistoricalInvocation.COLUMN_WEBHOOK_ID,
                AoHistoricalInvocation.COLUMN_OUTCOME
        }),
        @Index(name = AoHistoricalInvocation.INDEX_FINISH, methodNames = {
                AoHistoricalInvocation.COLUMN_FINISH,
        })
})
@Table(AoHistoricalInvocation.TABLE_NAME)
public interface AoHistoricalInvocation extends RawEntity<String> {

    String COLUMN_ERROR_CONTENT = "ERROR_CONTENT";
    String COLUMN_EVENT_ID = "EVENT_ID";
    String COLUMN_FINISH = "FINISH";
    String COLUMN_ID = "ID";
    String COLUMN_OUTCOME = "OUTCOME";
    String COLUMN_REQUEST_BODY = "REQUEST_BODY";
    String COLUMN_REQUEST_HEADERS = "REQUEST_HEADERS";
    String COLUMN_REQUEST_ID = "REQUEST_ID";
    String COLUMN_REQUEST_METHOD = "REQUEST_METHOD";
    String COLUMN_REQUEST_URL = "REQUEST_URL";
    String COLUMN_RESULT_DESCRIPTION = "RESULT_DESCRIPTION";
    String COLUMN_RESPONSE_BODY = "RESPONSE_BODY";
    String COLUMN_RESPONSE_HEADERS = "RESPONSE_HEADERS";
    String COLUMN_START = "START";
    String COLUMN_STATUS_CODE = "STATUS_CODE";
    String COLUMN_WEBHOOK_ID = "WEBHOOK_ID";

    String INDEX_WEBHOOK_EVENT_RESULT = "IDX_HIST_INVOKE_WER";
    String INDEX_WEBHOOK_RESULT = "IDX_HIST_INVOKE_WR";
    String INDEX_FINISH = "IDX_HIST_INVOKE_FIN";

    String TABLE_NAME = "HIST_INVOCATION";

    // lower-cased version of WebhookEvent.getId
    @Accessor(COLUMN_EVENT_ID)
    @StringLength(64)
    @NotNull
    String getEventId();

    /**
     * The primary key is defined as webhook-id.event-id.outcome-identifier, where outcome-identifier is E/F/S to
     * indicate error/failure/success. This format ensures that a compound unique constraint is maintained on
     * (webhook-id, event-id, outcome), which AO unfortunately does not support.
     * <p>
     * This approach is a bit ugly, but works and doesn't cost us much since we need a primary key anyway.
     */
    @PrimaryKey(COLUMN_ID)
    @StringLength(77) // 10 for webhook id + 64 for event id + 1 for outcome + 2 separators
    @NotNull
    String getId();

    @Transient
    @Accessor(COLUMN_ERROR_CONTENT)
    @StringLength(UNLIMITED)
    String getErrorContent();

    @Accessor(COLUMN_FINISH)
    @NotNull
    long getFinish();

    @Accessor(COLUMN_OUTCOME)
    @NotNull
    InvocationOutcome getOutcome();

    @Accessor(COLUMN_REQUEST_BODY)
    @StringLength(UNLIMITED)
    String getRequestBody();

    @Accessor(COLUMN_REQUEST_HEADERS)
    @StringLength(UNLIMITED)
    String getRequestHeaders();

    @Accessor(COLUMN_REQUEST_ID)
    @StringLength(64)
    @NotNull
    String getRequestId();

    @Accessor(COLUMN_REQUEST_METHOD)
    @StringLength(16)
    @NotNull
    String getRequestMethod();

    @Accessor(COLUMN_REQUEST_URL)
    @NotNull
    String getRequestUrl();

    @Transient
    @Accessor(COLUMN_RESPONSE_BODY)
    @StringLength(UNLIMITED)
    String getResponseBody();

    @Accessor(COLUMN_RESPONSE_HEADERS)
    @StringLength(UNLIMITED)
    String getResponseHeaders();

    @Accessor(COLUMN_RESULT_DESCRIPTION)
    @NotNull
    String getResultDescription();

    @Accessor(COLUMN_START)
    @NotNull
    long getStart();

    @Accessor(COLUMN_STATUS_CODE)
    Integer getStatusCode();

    @Accessor(COLUMN_WEBHOOK_ID)
    @NotNull
    int getWebhookId();
}
