package com.atlassian.webhooks.internal.model;

import com.atlassian.webhooks.WebhookEvent;
import com.atlassian.webhooks.WebhookEventProvider;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * Implementation of {@link WebhookEvent} that is used to represent event types that are not or no longer
 * provided by one of the registered {@link WebhookEventProvider providers}.
 *
 */
public class UnknownWebhookEvent implements WebhookEvent {

    private final String id;

    public UnknownWebhookEvent(String id) {
        this.id = requireNonNull(id, "id");
    }

    @Nonnull
    @Override
    public String getId() {
        return id;
    }

    @Nonnull
    @Override
    public String getI18nKey() {
        // since the webhook event type is not recognized, no i18n key is available. The best we can do is to simply
        // return the id.
        return id;
    }
}
