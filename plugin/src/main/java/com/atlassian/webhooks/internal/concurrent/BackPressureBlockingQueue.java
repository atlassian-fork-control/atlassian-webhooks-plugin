package com.atlassian.webhooks.internal.concurrent;

import java.util.Collection;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * A {@link java.util.concurrent.BlockingQueue} that provides a bit of back-pressure when the queue is full. This is
 * useful when used with as the task queue of a fixed-size executor. In that case, if the task queue is full, the
 * thread that tries to schedule or execute a task on the executor is delayed for a short time, thereby slowing down
 * the rate of task production.
 *
 * @param <T> the type of elements held in this queue
 */
public class BackPressureBlockingQueue<T> extends LinkedBlockingQueue<T> {

    private long offerTimeoutMs = 100L ;

    public BackPressureBlockingQueue() {
    }

    public BackPressureBlockingQueue(int capacity) {
        super(capacity);
    }

    public BackPressureBlockingQueue(Collection<? extends T> c) {
        super(c);
    }

    @Override
    public boolean offer(T t) {
        try {
            return super.offer(t, offerTimeoutMs, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        return false;
    }

    public void setOfferTimeoutMs(long timeoutMs) {
        offerTimeoutMs = Math.max(0, timeoutMs);
    }
}
