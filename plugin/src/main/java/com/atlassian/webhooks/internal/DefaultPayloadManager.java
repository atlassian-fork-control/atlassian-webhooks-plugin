package com.atlassian.webhooks.internal;

import com.atlassian.webhooks.WebhookInvocation;
import com.atlassian.webhooks.WebhookPayloadBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;

@Component
public class DefaultPayloadManager implements WebhookPayloadManager {

    private static final Logger log = LoggerFactory.getLogger(DefaultPayloadManager.class);

    private final WebhookHostAccessor hostAccessor;

    @Autowired
    public DefaultPayloadManager(WebhookHostAccessor hostAccessor) {
        this.hostAccessor = hostAccessor;
    }

    @Override
    public void setPayload(@Nonnull WebhookInvocation invocation, @Nonnull WebhookPayloadBuilder builder) {
        hostAccessor.getPayloadProviders()
                .stream()
                .filter(marshaller -> {
                    boolean supports = marshaller.supports(invocation);
                    if (log.isTraceEnabled()) {
                        log.trace("Results of marshaller [{}] supporting is [{}] for invocation [{}]",
                                marshaller.getClass().getSimpleName(), supports, invocation.getId());
                    }
                    return supports;
                })
                .findFirst()
                .ifPresent(payloadProvider -> {
                    if (log.isDebugEnabled()) {
                        log.debug("Webhook payload has been set by [{}] for invocation [{}]",
                                payloadProvider.getClass().getSimpleName(), invocation.getId());
                    }
                    payloadProvider.setPayload(invocation, builder);
                });

    }
}
