package com.atlassian.webhooks.internal;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.webhooks.*;
import com.atlassian.webhooks.internal.model.UnknownWebhookEvent;
import com.google.common.collect.ImmutableList;
import io.atlassian.util.concurrent.ThreadFactories;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.PreDestroy;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.function.Function;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;

@Component
@ExportAsService(LifecycleAware.class)
public class OsgiWebhookHostAccessor implements WebhookHostAccessor, LifecycleAware {

    private final Object lock = new Object();

    private final BundleContext bundleContext;
    private final ServiceTracker<WebhooksConfiguration, WebhooksConfiguration> configurationTracker;
    private final ServiceTracker<WebhookRequestEnricher, WebhookRequestEnricher> enricherTracker;
    private final SortedSet<WebhookRequestEnricher> enrichers;
    private final ServiceTracker<WebhookEventProvider, WebhookEventProvider> eventProviderTracker;
    private final SortedSet<WebhookEventProvider> eventProviders;
    private final ServiceTracker<ScheduledExecutorService, ScheduledExecutorService> executorServiceTracker;
    private final ServiceTracker<WebhookFilter, WebhookFilter> filterTracker;
    private final SortedSet<WebhookFilter> filters;
    private final ServiceTracker<WebhookPayloadProvider, WebhookPayloadProvider> payloadProviderTracker;
    private final SortedSet<WebhookPayloadProvider> payloadProviders;

    private volatile ScheduledExecutorService executorService;
    private volatile boolean stopped;

    @Autowired
    public OsgiWebhookHostAccessor(BundleContext bundleContext) {
        this.bundleContext = bundleContext;

        configurationTracker = new ServiceTracker<>(bundleContext, WebhooksConfiguration.class, null);
        configurationTracker.open();
        enrichers = new ConcurrentSkipListSet<>(getComparator(WebhookRequestEnricher::getWeight));
        enricherTracker = new ServiceTracker<>(bundleContext, WebhookRequestEnricher.class,
                new CachingCustomiser<>(enrichers));
        enricherTracker.open();
        eventProviders = new ConcurrentSkipListSet<>(getComparator(WebhookEventProvider::getWeight));
        eventProviderTracker = new ServiceTracker<>(bundleContext, WebhookEventProvider.class,
                new CachingCustomiser<>(eventProviders));
        eventProviderTracker.open();
        filters = new ConcurrentSkipListSet<>(getComparator(WebhookFilter::getWeight));
        filterTracker = new ServiceTracker<>(bundleContext, WebhookFilter.class,
                new CachingCustomiser<>(filters));
        filterTracker.open();

        payloadProviders = new ConcurrentSkipListSet<>(getComparator(WebhookPayloadProvider::getWeight));
        payloadProviderTracker = new ServiceTracker<>(bundleContext, WebhookPayloadProvider.class,
                new CachingCustomiser<>(payloadProviders));
        payloadProviderTracker.open();

        executorServiceTracker = new ServiceTracker<>(bundleContext, ScheduledExecutorService.class, null);
    }

    @PreDestroy
    public void destroy() {
        enricherTracker.close();
        eventProviderTracker.close();
        filterTracker.close();
        payloadProviderTracker.close();
        configurationTracker.close();
    }

    @Nonnull
    @Override
    public Optional<WebhooksConfiguration> getConfiguration() {
        return ofNullable(configurationTracker.getService());
    }

    @Nonnull
    @Override
    public Collection<WebhookRequestEnricher> getEnrichers() {
        return enrichers;
    }

    @Nonnull
    @Override
    public WebhookEvent getEvent(@Nonnull String id) {
        requireNonNull(id, "eventId");

        for (WebhookEventProvider provider : eventProviders) {
            WebhookEvent type = provider.forId(id);
            if (type != null) {
                return type;
            }
        }

        // no provider knows about "id"
        return new UnknownWebhookEvent(id);
    }

    @Nonnull
    @Override
    public List<WebhookEvent> getEvents() {
        ImmutableList.Builder<WebhookEvent> builder = ImmutableList.builder();
        eventProviders.stream()
                .map(WebhookEventProvider::getEvents)
                .filter(Objects::nonNull)
                .flatMap(List::stream)
                .filter(Objects::nonNull)
                .sorted(Comparator.comparing(WebhookEvent::getId))
                .forEach(builder::add);
        return builder.build();
    }

    @Nonnull
    @Override
    public ScheduledExecutorService getExecutorService() {
        if (executorService != null) {
            return executorService;
        }

        // try to get the executor service from OSGI
        ScheduledExecutorService result = executorServiceTracker.getService();
        if (result != null) {
            return result;
        }

        // no executor service available from OSGI, create one for the plugin
        synchronized (lock) {
            if (stopped) {
                throw new IllegalStateException("The webhooks plugin has been stopped");
            }
            if (executorService == null) {
                executorService = new ScheduledThreadPoolExecutor(1,
                        ThreadFactories.namedThreadFactory("atlassian-webhooks-scheduler", ThreadFactories.Type.DAEMON));
            }

            return executorService;
        }
    }

    @Nonnull
    @Override
    public Collection<WebhookFilter> getFilters() {
        return filters;
    }

    @Nonnull
    @Override
    public Collection<WebhookPayloadProvider> getPayloadProviders() {
        return payloadProviders;
    }

    @Override
    public void onStart() {
        stopped = false;
    }

    @Override
    public void onStop() {
        synchronized (lock) {
            stopped = true;

            if (executorService != null) {
                executorService.shutdown();
                executorService = null;
            }
        }
    }

    private static <T> Comparator<T> getComparator(Function<T, Integer> weightFunction) {
        return (T o1, T o2) -> {
            int weight = weightFunction.apply(o1).compareTo(weightFunction.apply(o2));
            if (weight != 0) {
                return weight;
            }

            // In this case we've hit two services with the same weight. We want to keep it deterministic so lets
            // order by class name. This also has the advantage of still identifiying duplicates.
            return o1.getClass().getCanonicalName().compareTo(o2.getClass().getCanonicalName());
        };
    }

    private class CachingCustomiser<T> implements ServiceTrackerCustomizer<T, T> {

        private final SortedSet<T> cache;

        private CachingCustomiser(SortedSet<T> cache) {
            this.cache = cache;
        }

        @Override
        public T addingService(ServiceReference<T> serviceReference) {
            T service = bundleContext.getService(serviceReference);
            cache.add(service);

            return service;
        }

        @Override
        public void modifiedService(ServiceReference<T> serviceReference, T t) {
            // ignore; we don't care about the properties on the serviceReference
        }

        @Override
        public void removedService(ServiceReference<T> serviceReference, T t) {
            cache.remove(t);

            bundleContext.ungetService(serviceReference);
        }
    }
}
