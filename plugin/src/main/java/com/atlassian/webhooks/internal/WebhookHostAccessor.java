package com.atlassian.webhooks.internal;

import com.atlassian.webhooks.*;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ScheduledExecutorService;

public interface WebhookHostAccessor {

    @Nonnull
    Optional<WebhooksConfiguration> getConfiguration();

    @Nonnull
    Collection<WebhookRequestEnricher> getEnrichers();

    @Nonnull
    WebhookEvent getEvent(@Nonnull String id);

    @Nonnull
    List<WebhookEvent> getEvents();

    /**
     * @since 6.1
     */
    @Nonnull
    ScheduledExecutorService getExecutorService();

    @Nonnull
    Collection<WebhookFilter> getFilters();

    @Nonnull
    Collection<WebhookPayloadProvider> getPayloadProviders();
}
