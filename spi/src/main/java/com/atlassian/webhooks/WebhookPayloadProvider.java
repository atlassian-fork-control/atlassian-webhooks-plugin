package com.atlassian.webhooks;

import javax.annotation.Nonnull;

/**
 * A body marshaller is used to transform an invocation into a binary byte array to represent the content that should be
 * sent in the upcoming HTTP request.
 *
 * @since 5.0
 */
public interface WebhookPayloadProvider {

    /**
     * Gets the weight used to order the marshallers. Lower weights are processed first
     * <p>
     * Values below 1000 are reserved for internal use.
     *
     * @return the weight
     */
    int getWeight();

    /**
     * Set the body content and headers for the specific invocation
     *
     * @param invocation webhook invocation
     */
    void setPayload(@Nonnull WebhookInvocation invocation, @Nonnull WebhookPayloadBuilder builder);

    /**
     * @param invocation webhook invocation
     * @return {@code true} if this marshaller can process the webhook invocation, {@code false} otherwise
     */
    boolean supports(@Nonnull WebhookInvocation invocation);
}
