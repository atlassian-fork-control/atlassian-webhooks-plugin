# Atlassian WebHooks Plugin - Server/DC

## Description

This is an Atlassian Plugin that allows products and plugins as providers to define events as webhooks. As consumers
they can register against web hooks and as such be notified via an HTTP POST request when the event corresponding to the
web hook is fired.

## Atlassian Developer?

### Committing Guidelines

Please see [The Platform Rules of Engagement (go/proe)](http://go.atlassian.com/proe) for committing to this module.

### Builds

The Bamboo builds for this project are on [Server Syd Bamboo](https://server-syd-bamboo.internal.atlassian.com/browse/WEBHOOKS-MASTER)

## External User?

### Issues

Please raise any issues you find with this module in [JIRA](https://ecosystem.atlassian.net/browse/WEBHOOKS)

### Forking

This plugin is forked. The cloud fork can be found at https://bitbucket.org/atlassian/cloud-atlassian-webhooks-plugin

## Documentation

The Atlassian Webhooks library provides the building blocks needed to implement webhooks support in Atlassian products.
In a nutshell, the library supports:

- registering/unregistering webhooks to receive HTTP callbacks when certain webhook events (e.g. `project:created`) occur.
- publishing webhooks by the host application and plugins. The framework will queue the webhook event, and at a later
  time dispatch the webhook event to all registered webhooks. The dispatching of webhooks is asynchronous.
- registering filters, enrichers and payload providers to customize and extend the webhook functionality 

### Registering a webhook

Webhooks are registered and unregistered using the `WebhookService`. When registering a new webhook, the caller can 
provide:

* one or more webhook events (e.g. `space_created`)
* the URL the webhook callbacks should be sent to
* a name, used to help identify the purpose of the webhook
* one or more scopes. A scope identifies a 'container' of entities that webhooks can subscribe to. The 
  `WebhookScope.GLOBAL` scope is the only pre-defined scope. Different products can add additional scopes. For example,
  Bitbucket supports registering webhooks for particular repositories.
* custom configuration that may be used by a custom `WebhookFilter` or `WebhookRequestEnricher` provided by the host
  application. Examples might be a JQL condition that the issue should match, a flag to include a digital signature on
  the webhook header, etc.
  
### Publishing   

`WebhookService` is also used to publish a webhook, typically in response to an application event. `WebhookService`
will look up all webhook subscribers for the `WebhookEvent` and scope(s) provided and deliver a webhook HTTP callback
asynchronously.

```java
    @EventListener
    public void onSpaceCreated(SpaceCreateEvent event) {
        Object payload = buildWebhookPayload(event);    
        webhookService.publish(WebhookPublishRequest.builder(ApplicationWebHookEvent.SPACE_CREATED, payload)
                .scopes(WebhookScope.GLOBAL, new SpaceScope(event.getSpace().getId()))
                .build());
    }
```    


### Integrating atlassian-webhooks with a host application

The host application needs to provide a number of classes to `atlassian-webhooks` to make the magic happen. The code
snippets that follow use Confluence as an example.

Note: In the examples below, various SPI interfaces include a `int getWeight()` method. This weight is used by the
framework to sort the registered instances before calling them. The instance with the lowest weight is called first. 

#### 1. Bundle atlassian-webhooks-plugin and export atlassian-webhooks-api from the host application

Add `atlassian-webhooks-plugin` as a bundled plugin to the host application. Add a dependency on
`atlassian-webhooks-api` to the host application and export the `com.atlassian.webhooks.*` packages from the system
bundle. This ensures that webhooks REST endpoints can be added to the core REST API without any issues if the webhooks
plugin fails to start for some reason.

#### 2. Create a new bundled plugin for webhooks support

Create a new bundled plugin that provides the SPI parts that atlassian-webhooks needs, e.g. `confluence-webhooks`. Make
sure you add `provided` dependencies on `atlassian-webhooks-api` and `atlassian-webhooks-spi`. At runtime the classes 
in these modules are provided (over OSGi) by the `atlassian-webhooks-plugin`.

#### 3. Register custom webhooks

First, register all supported `WebhookEvent` instances in the new bundled plugin. The `WebhookEvent` interface is 
minimal; it just provides an ID and an i18n key for the webhook. An `enum` is a good way to define these webhook event 
instance: 

```java
public enum ApplicationWebhookEvent implements WebhookEvent {
    SPACE_CREATED("space_created"),
    SPACE_REMOVED("space_removed"),
    SPACE_UPDATED("space_updated");
        
    private static final String I18N_PREFIX = "confluence.webhooks.event.";

    private final String id;
    private final String i18nKey;

    ApplicationWebhookEvent(String id) {
        this.id = id;
        this.i18nKey = I18N_PREFIX + name().toLowerCase();
    }

    @Nonnull
    @Override
    public String getId() {
        return id;
    }

    @Nonnull
    @Override
    public String getI18nKey() {
        return i18nKey;
    }
    
    @Nullable
    public static ApplicationWebhookEvent forId(@Nullable String id) {
        for (ApplicationWebhookEvent event : values()) {
            if (event.getId().equalsIgnoreCase(id)) {
                return event;
            }
        }
        return null;
    }
} 
```

Add descriptions in an i18n properties file, e.g. `i18n/confluence-webhooks.properties`:

```
confluence.webhooks.event.space_created=Space created
confluence.webhooks.event.space_removed=Space removed
confluence.webhooks.event.space_updated=Space updated
```

and register the file in your `atlassian-plugin.xml`:

```xml
<resource type="i18n" name="i18n" location="i18n/confluence-webhooks"/>
```

The host application registers these webhook events with the framework by implementing the `WebhookEventProvider` 
interface and making an instance available over OSGi.

```java
@Component
@ExportAsService(WebhookEventProvider.class)
public class ApplicationWebhookEventProvider implements WebhookEventProvider {

    private static final List<WebhookEvent> EVENTS = ImmutableList.copyOf(ApplicationWebhookEvent.values());

    @Override
    public WebhookEvent forId(@Nonnull String id) {
        return ApplicationWebhookEvent.forId(id);
    }

    @Nonnull
    @Override
    public List<WebhookEvent> getEvents() {
        return EVENTS;
    }

    @Override
    public int getWeight() {
        return 10;
    }
}
``` 

This instance can be made available in OSGi using the standard mechanisms (using `<osgi:service>` in the Spring context,
using `plugin:available="true"` on Spring beans in the core Spring context, using `@AvailableToPlugins` or using 
`@ExportAsService` when using spring-scanner in a plugin).

#### 4. Publish webhooks on application events

Finally, it's time to publish some webhooks. Usually, webhooks should be published when an application event happens. 
To make this happen, add a class that publishes webhooks in response to application events:

```java
@Component
public class ApplicationWebhookEventPublisher {

    private static final Logger log = LoggerFactory.getLogger(ApplicationWebhookEventPublisher.class);

    private final ApiRestEntityFactory apiRestEntityFactory;
    private final WebhookService webhookService;

    @Autowired
    public ApplicationWebhookEventPublisher(@ComponentImport ApiRestEntityFactory apiRestEntityFactory,
                                            @ComponentImport WebhookService webhookService) {
        this.apiRestEntityFactory = apiRestEntityFactory;
        this.webhookService = webhookService;
    }

    @EventListener
    public void onSpaceCreated(SpaceCreateEvent event) {
        publishSpaceWebhook(ApplicationWebhookEvent.SPACE_CREATED,
                new SpaceCreatedJsonEvent(event.getTimestamp(), toApiEntity(event.getSpace())));
    }

    @EventListener
    public void onSpaceRemoved(SpaceRemoveEvent event) {
        publishSpaceWebhook(ApplicationWebhookEvent.SPACE_REMOVED,
                new SpaceRemovedJsonEvent(event.getTimestamp(), toApiEntity(event.getSpace())));
    }

    @EventListener
    public void onSpaceUpdated(SpaceUpdateEvent event) {
        publishSpaceWebhook(ApplicationWebhookEvent.SPACE_UPDATED,
                new SpaceUpdatedJsonEvent(event.getTimestamp(), toApiEntity(event.getSpace()),
                        toApiEntity(event.getOriginalSpace())));
    }

    private void publishSpaceWebhook(WebhookEvent webhookEvent, SpaceJsonEvent event) {
        Space space = event.getSpace();
        log.trace("Publishing webhook {} for space {}", webhookEvent.getId(), space.getKey());
        webhookService.publish(WebhookPublishRequest.builder(webhookEvent, event)
                .scopes(WebhookScope.GLOBAL, new SpaceScope(space.getId()))
                .build());
    }

    private Space toApiEntity(com.atlassian.confluence.spaces.Space space) {
        RestEntity<Space> entity = apiRestEntityFactory.buildRestEntityFrom(space, Expansions.EMPTY);
        return entity == null ? null : entity.getDelegate();
    }
}
```

A note about the Confluence implementation listed above: webhooks are dispatched asynchronously and the HTTP body 
is generated from the webhook payload on the async dispatch thread. To prevent issues with lazy loading Hibernate 
entities, the event is translated into value objects _before_ being published to the webhook.

#### 5. Map the payload to the webhook body 

Next, we need to tell the webhooks framework how to build the HTTP request body for a given webhook by implementing the
`WebhookPayloadProvider` interface and making it available as an OSGi service.

```java
@Component
@ExportAsService(WebhookPayloadProvider.class)
public class ApplicationWebhookPayloadProvider implements WebhookPayloadProvider {

    private static final Logger log = LoggerFactory.getLogger(ApplicationWebhookPayloadProvider.class);
    private static final byte[] EMPTY_BODY = new byte[0];
    private static final ObjectMapper OBJECT_MAPPER  = new ObjectMapper();

    @Override
    public int getWeight() {
        return 100;
    }

    @Override
    public void setPayload(WebhookInvocation invocation, WebhookPayloadBuilder builder) {
        if (invocation.getEvent() instanceof ApplicationWebhookEvent) {
            byte[] body = invocation.getPayload()
                    .map(this::toJson)
                    .map(bodyString -> bodyString.getBytes(StandardCharsets.UTF_8))
                    .orElse(EMPTY_BODY);

            builder.body(body, MediaType.JSON_UTF_8.toString());
        }
    }

    @Override
    public boolean supports(WebhookInvocation invocation) {
        return invocation.getEvent() instanceof ApplicationWebhookEvent;
    }

    private String toJson(Object payload) {
        try {
            return OBJECT_MAPPER.writeValueAsString(payload);
        } catch (IOException e) {
            log.warn("Unexpected exception while rendering an event object to a JSON string", e);
        }

        return "";
    }
}
```

#### 6. Recommended - Expose a REST admin endpoint for each supported scope

The webhooks framework does not expose a REST API for managing webhooks from the webhooks plugin itself, but makes it
easy to add one or more endpoints to the core REST API module. The webhooks plugin does not expose a REST API because 
it makes sense to expose a REST endpoint for each supported webhook's scope. For instance, if the host product wants to
support subscribing to webhooks at the project-level (and only receive webhooks for events that happen inside that 
project), it's natural to expose that administration endpoint at `rest/api/latest/projects/<project-key>/webhooks`. In
addition, _global_ webhooks could be administered under `rest/api/latests/webhooks`.

The `atlassian-webhooks-rest` module contains `AbstractWebhooksResource`, which contains all logic for webhooks 
administration. The host application can provide an endpoint that extends `AbstractWebhooksResource` and adds 
permission checks and `@Path` mappings. The example below shows a few REST methods:

```java
@AnonymousAllowed
@Produces({MediaType.APPLICATION_JSON})
@Path("/webhooks")
public class GlobalWebhooksResource extends AbstractWebhooksResource {

    private final OptionalServiceProvider optionalServiceProvider;
    private final PermissionEnforcer permissionEnforcer;

    public GlobalWebhooksResource(@ComponentImport I18nResolver i18nResolver,
                                  @ComponentImport PermissionEnforcer permissionEnforcer,
                                  OptionalServiceProvider optionalServiceProvider) {
        super(i18nResolver, new SimpleRestResponseBuilder());

        this.optionalServiceProvider = optionalServiceProvider;
        this.permissionEnforcer = permissionEnforcer;
    }

    /**
     * Create a webhook via the URL.
     * <p>
     * The authenticated user must be an administrator to call this resource.
     *
     * @param webhook the webhook to be created
     *
     * @request.representation.example {@link RestWebhook#EXAMPLE}
     * @response.representation.200.mediaType application/json
     * @response.representation.200.qname webhook
     * @response.representation.200.example {@link RestWebhook#EXAMPLE}
     * @response.representation.200.doc A created webhook.
     * @response.representation.400.mediaType application/json
     * @response.representation.400.qname errors
     * @response.representation.400.example {@link RestErrors#EXAMPLE}
     * @response.representation.400.doc The webhook parameters were invalid or not supplied.
     * @response.representation.401.mediaType application/json
     * @response.representation.401.qname errors
     * @response.representation.401.example {@link RestErrors#EXAMPLE}
     * @response.representation.401.doc The currently authenticated user has insufficient permissions to create webhooks
     */
    @POST
    public Response createWebhook(@Context UriInfo uriInfo, RestWebhook webhook) {
        return internalCreateWebhook(uriInfo, WebhookScope.GLOBAL, webhook);
    }

    /**
     * Delete a webhook via the URL.
     * <p>
     * The authenticated user must be an administrator to call this resource.
     *
     * @param webhookId the id of the webhook to be deleted.
     *
     * @response.representation.401.mediaType application/json
     * @response.representation.401.qname errors
     * @response.representation.401.example {@link RestErrors#EXAMPLE}
     * @response.representation.401.doc The currently authenticated user has insufficient permissions to delete webhooks.
     * @response.representation.404.mediaType application/json
     * @response.representation.404.qname errors
     * @response.representation.404.example {@link RestErrors#EXAMPLE}
     * @response.representation.404.doc The webhook does not exist.
     */
    @DELETE
    @Path("/{webhookId}")
    public Response deleteWebhook(@PathParam("webhookId") int webhookId) {
        return internalDeleteWebhook(WebhookScope.GLOBAL, webhookId);
    }

    @Override
    protected InvocationHistoryService getInvocationHistoryService() {
        return optionalServiceProvider.getInvocationHistoryService()
                .map(InvocationHistoryService.class::cast)
                .orElseThrow(NotFoundException::new);
    }

    @Override
    protected WebhookService getWebhookService() {
        return optionalServiceProvider.getWebhookService()
                .map(WebhookService.class::cast)
                .orElseThrow(NotFoundException::new);
    }

    @Override
    protected void validateCanAdmin(WebhookScope scope) {
        // only site admins can administer webhooks
        permissionEnforcer.enforceAdmin();
    }

    private static class SimpleRestResponseBuilder implements RestResponseBuilder {

        @Override
        public Object error(String fieldName, String errorMessage) {
            return ImmutableMap.of("message", errorMessage);
        }

        @Override
        public Object page(UriInfo uriInfo, int start, int limit, List<?> values, boolean hasMore) {
            return RestList.newRestList()
                    .pageRequest(new RestPageRequest(uriInfo, start, limit))
                    .results((List) values, hasMore)
                    .build();
        }
    }
}
```

The `OptionalServiceProvider` in the example above is used to ensure that the core REST plugin loads, even if the
webhooks plugin does not start for some reason. The `OptionalServiceProvider` attempts to retrieve the `WebhookService`
and `InvocationHistoryService` from OSGi, but does not fail if these services and the relevant webhooks classes are not
available.

```java
@Component("optionalServiceProvider")
public class OsgiOptionalServiceProvider implements OptionalServiceProvider {

    private final ServiceTracker<Object, Object> invocationHistoryServiceTracker;
    private final ServiceTracker<Object, Object> webhooksServiceTracker;

    @Autowired
    public OsgiOptionalServiceProvider(BundleContext bundleContext) {
        // use the class names (instead of the classes) to prevent loading the webhooks classes early. If the classes
        // are not available, we want to return null - and not throw ClassNotFoundExceptions 
        invocationHistoryServiceTracker = new ServiceTracker<>(bundleContext, "com.atlassian.webhooks.history.InvocationHistoryService", null);
        webhooksServiceTracker = new ServiceTracker<>(bundleContext, "com.atlassian.webhooks.WebhookService", null);

        invocationHistoryServiceTracker.open();
        webhooksServiceTracker.open();
    }

    @PreDestroy
    public void onDestroy() {
        invocationHistoryServiceTracker.close();
        webhooksServiceTracker.close();
    }

    @Override
    public Optional<Object> getInvocationHistoryService() {
        return Optional.ofNullable(invocationHistoryServiceTracker.getService());
    }

    @Override
    public Optional<Object> getWebhookService() {
        return Optional.ofNullable(webhooksServiceTracker.getService());
    }
}
```  

Further care needs to be taken in the rest module's `pom.xml` to ensure that the bundle loads and starts, even if the
webhooks bundle fails.

Add the following to the `dependencies` section of the pom to bundle the `atlassian-webhooks-rest` module and make
the `atlassian-webhooks-api` classes available without bundling them (the API classes will be provided by the 
webhooks plugin at runtime).

```xml
<!-- Bundle the atlassian-webhooks-rest classes in this module. The module contains a base implementation
     for a webhook resource, including REST representations of key webhook entities. There's also a dependency
     on atlassian-webhooks-api, but this should _not_ be bundled as these classes should be made available
     in OSGi by atlassian-webhooks-plugin.
-->
<dependency>
    <groupId>com.atlassian.webhooks</groupId>
    <artifactId>atlassian-webhooks-api</artifactId>
    <scope>provided</scope>
</dependency>
<dependency>
    <groupId>com.atlassian.webhooks</groupId>
    <artifactId>atlassian-webhooks-rest</artifactId>
</dependency>
``` 

Add the following to the `Import-Package` section of the `maven-amps-plugin` to make the webhooks imports optional:

```xml
<Import-Package>
    .....
    com.atlassian.webhooks;resolution:=optional,
    com.atlassian.webhooks.diagnostics;resolution:=optional,
    com.atlassian.webhooks.history;resolution:=optional,
    com.atlassian.webhooks.request;resolution:=optional,
    com.atlassian.webhooks.util;resolution:=optional,
    .....
</Import-Package>
```

#### 7. Recommended - WebhooksConfiguration

The new bundled plugin can register a custom `WebhooksConfiguration` instance to configure the webhooks framework. All
methods on `WebhooksConfiguration` have a default implementation, so the host application only has to override the
methods for the settings that it wants to change. Usually, the host application will want to allow system administrators
to control some settings through a config file. In this case, the custom `WebhooksConfiguration` should read the 
config values from the config file to expose them to the webhooks framework.

```java
@Component("webhooksConfiguration")
@ExportAsService(WebhooksConfiguration.class)
public class ApplicationWebhooksConfiguration implements WebhooksConfiguration {

    @Override
    public boolean isStatisticsEnabled() {
        return true;
    }

    @Override
    public boolean isInvocationHistoryEnabled() {
        return true;
    }
}
```

#### 8. Optional - Enrich webhooks

The host application can optionally register one or more `WebhookRequestEnricher` services. These enrichers are called
just before a webhook is dispatched and allows the host application to inject custom headers into the HTTP request or
register asynchronous callbacks to handle webhook success or failure.

Here's a very simple enricher that injects a custom header and logs information about the dispatched webhook.

```java
@Component
@ExportAsService(WebhookRequestEnricher.class)
public class LoggingWebhookRequestEnricher implements WebhookRequestEnricher {

    private static final Logger log = LoggerFactory.getLogger(LoggingWebhookRequestEnricher.class);

    @Override
    public void enrich(@Nonnull WebhookInvocation invocation) {
        invocation.getRequestBuilder().header("X-Example-Header", "some value");
        invocation.registerCallback(new LoggingCallback());
    }

    @Override
    public int getWeight() {
        return 100;
    }

    private static class LoggingCallback implements WebhookCallback {

        @Override
        public void onError(WebhookHttpRequest request, @Nonnull Throwable throwable,
                            @Nonnull WebhookInvocation invocation) {
            log.warn("Webhook {} to {} failed",
                    invocation.getEvent().getId(), invocation.getWebhook().getUrl(), throwable);
        }

        @Override
        public void onFailure(@Nonnull WebhookHttpRequest request,
                              @Nonnull WebhookHttpResponse response,
                              @Nonnull WebhookInvocation invocation) {
            log.warn("Webhook {} to {} responded with status {}",
                    invocation.getEvent().getId(), invocation.getWebhook().getUrl(), response.getStatusCode());
        }

        @Override
        public void onSuccess(@Nonnull WebhookHttpRequest request,
                              @Nonnull WebhookHttpResponse response,
                              @Nonnull WebhookInvocation invocation) {
            log.warn("Webhook {} to {} responded with status {}",
                    invocation.getEvent().getId(), invocation.getWebhook().getUrl(), response.getStatusCode());
        }
    }
}
```

#### 9. Optional - Register `WebhookFilter`

The host application may choose to allow custom filters to be defined on registered webhooks. For instance, Jira 
supports specifying a JQL condition on a webhook subscription to only receive webhooks for issues that match the JQL 
query.

Such custom matching conditions can be implemented by storing the JQL query in the webhook configuration, and 
registering a `WebhookFilter` that compares the webhook payload with the JQL condition and decides whether the webhook
should be sent.

```java
@Component
@ExportAsService
public class JQLWebhookFilter implements WebhookFilter {
    @Override
    public boolean filter(@Nonnull WebhookInvocation invocation) {
        Issue issue = getIssueFromPayload(invocation.getPayload());
        if (issue == null) {
            // no issue in the payload, nothing to match. Let the webhook go through
            return true;
        }
        String jql = invocation.getWebhook().getConfiguration().get("jql");
        // allow the webhook to go through if there's no JQL condition, or if the issue matches the JQL
        return (StringUtils.isBlank(jql) || matchesJql(issue, jql));
    }

    @Override
    public int getWeight() {
        return 100;
    }
    
    private Issue getIssueFromPayload(Optional<Object> payload) {
        ...
    }
    
    private boolean matchesJql(Issue issue, String jql) {
        ...
    }
}
```
  

### Defining new webhooks

Both the host application and plugins can register new webhooks event types and publish these webhooks when an 
application event happens. To do this, the plugin needs to publish an additional `WebhookEventProvider` and
`WebhookPayloadProvider` and an eventlistener that publishes webhooks in response to application events.

The steps involved are covered in steps 3-5 in the section above. 


