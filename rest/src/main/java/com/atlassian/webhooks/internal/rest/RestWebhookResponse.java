package com.atlassian.webhooks.internal.rest;

import com.atlassian.webhooks.request.WebhookHttpResponse;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Map;

@JsonSerialize
public class RestWebhookResponse extends LinkedHashMap<String, Object> {

    public static RestWebhookResponse EXAMPLE = new RestWebhookResponse(
            200, ImmutableMap.of("Content-Type", "application/json"), "{\n" +
            "  \"accepted\": true\n" +
            "}");

    private final String BODY = "body";
    private final String HEADERS = "headers";
    private final String STATUS = "status";

    @SuppressWarnings("unused") //Required by Jersey
    public RestWebhookResponse() {
    }

    public RestWebhookResponse(WebhookHttpResponse response) {
        put(STATUS, response.getStatusCode());
        put(HEADERS, response.getHeaders().getHeaders());

        try {
            put(BODY, IOUtils.toString(response.getBody().getContent(), StandardCharsets.UTF_8));
        } catch (IOException ex) {
            put(BODY, "Unable to set body");
        }
    }

    private RestWebhookResponse(int status, Map<String, String> headers, String body) {
        put(STATUS, status);
        put(HEADERS, headers);
        put(BODY, body);
    }
}
