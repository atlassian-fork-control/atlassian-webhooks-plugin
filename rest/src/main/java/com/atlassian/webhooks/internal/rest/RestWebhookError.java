package com.atlassian.webhooks.internal.rest;

import java.util.LinkedHashMap;

public class RestWebhookError extends LinkedHashMap<String, Object> {

    @SuppressWarnings("unused") //Required by Jersey
    public RestWebhookError() {
    }

    public RestWebhookError(String errorMessage) {
        put("message", errorMessage);
    }
}
