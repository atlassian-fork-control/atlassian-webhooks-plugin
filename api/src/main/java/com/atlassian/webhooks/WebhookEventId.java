package com.atlassian.webhooks;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Validate that the annotated string is a known {@link WebhookEvent} {@link WebhookEvent#getId() ID}.
 *
 * @since 5.0
 */
@Documented
@Constraint(validatedBy = { WebhookEventIdValidator.class })
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER })
@Retention(RUNTIME)
@ReportAsSingleViolation
public @interface WebhookEventId {

    String message() default "{webhooks.unknown.event}";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
