package com.atlassian.webhooks.request;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

/**
 * A HTTP client agnostic representation of a HTTP response body
 *
 * @since 5.0
 */
public interface WebhookResponseBody {

    /**
     * Retrieve a stream of the content that exists in the response body
     *
     * @return A data stream containing the body content
     * @throws IOException if the underlying input stream is closed, or other issues with creating the stream
     */
    @Nonnull
    InputStream getContent() throws IOException;

    /**
     * Return the content type associated with the body.
     *
     * If the 'Content-Type' header is not set, or the content itself is empty, this may be {@link Optional#empty()}
     *
     * @return the mime type of the response body if available
     */
    @Nonnull
    Optional<String> getContentType();
}
