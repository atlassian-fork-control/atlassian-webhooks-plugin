package com.atlassian.webhooks.request;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * A client agnostic representation of a HTTP request
 *
 * @since 5.0
 */
public interface WebhookHttpRequest {

    /**
     * Get the UTF-8 encoded bytes that make up the HTTP request
     *
     * @return the request content
     */
    @Nullable
    byte[] getContent();

    /**
     * Get the mime type for the request
     *
     * @return request mime type
     */
    @Nonnull
    Optional<String> getContentType();

    /**
     * Retrieve a list of headers that will be attached to the request
     *
     * @return headers for the request
     */
    @Nonnull
    Map<String, String> getHeaders();

    /**
     * Get the HTTP request method to perform this request as.
     *
     * @return request method
     */
    @Nonnull
    Method getMethod();

    /**
     * Retrieves all the query parameters that to be appended to the URL
     *
     * @return query parameters for the request
     */
    @Nonnull
    Map<String, List<String>> getQueryParameters();

    /**
     * Get the URL to make the request to. This should not include query parameters.
     *
     * @return the URL for the request
     */
    @Nonnull
    String getUrl();

    interface Builder {

        @Nonnull
        WebhookHttpRequest build();

        /**
         * @return the body of the HTTP request to send
         */
        @Nullable
        byte[] getBody();

        /**
         * @return the headers to be attached to the HTTP request
         */
        @Nonnull
        Map<String, String> getHeaders();

        /**
         * Get the method in which to perform the HTTP request
         *
         * @return HTTP request method
         */
        @Nonnull
        Method getMethod();

        /**
         * Get the query parameters added to the HTTP request. This should be an immutable copy
         *
         * @return key/value pair of query parameters
         */
        @Nonnull
        Map<String, List<String>> getParameters();

        /**
         * @return the URL to send the HTTP request to
         */
        @Nonnull
        String getUrl();

        /**
         * Sets or updates the value for an header
         *
         * @param name  name of header
         * @param value value of header
         */
        @Nonnull
        Builder header(@Nonnull String name, @Nonnull String value);

        /**
         * Set the method to perform the HTTP request as
         *
         * @param method request method
         */
        @Nonnull
        Builder method(@Nonnull Method method);

        /**
         * Add a query parameter to the HTTP request
         *
         * @param name   parameter name
         * @param values parameter values
         */
        @Nonnull
        Builder parameter(@Nonnull String name, @Nullable String... values);

        /**
         * Removes a header from the list of headers to be sent in the request
         *
         * @param name name of header to remove
         * @return {@code true} if a header was removed. {@code false} otherwise
         */
        @Nonnull
        Builder removeHeader(@Nonnull String name);

        /**
         * Allows changing the URL that this request is sent to
         *
         * @param url the updated URL
         */
        @Nonnull
        Builder url(@Nonnull String url);

        /**
         * Allows changing the URL that this request is sent to
         *
         * @param url the updated URL
         */
        @Nonnull
        Builder url(@Nonnull URI url);
    }
}
