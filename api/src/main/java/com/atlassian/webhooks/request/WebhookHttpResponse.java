package com.atlassian.webhooks.request;

import javax.annotation.Nonnull;
import java.io.Closeable;

/**
 * A HTTP client agnostic response to a request
 *
 * @since 5.0
 */
public interface WebhookHttpResponse extends Closeable {

    /**
     * Close the response. After closing, the body must not be used
     */
    @Override
    void close();

    /**
     * Get the body of the response
     *
     * @return a wrapper around the response body
     */
    @Nonnull
    WebhookResponseBody getBody();

    /**
     * Get the headers in the response.
     *
     * @return a wrapper around the response headers
     */
    @Nonnull
    WebhookResponseHeaders getHeaders();

    /**
     * Retrieve the HTTP status code for the response
     *
     * @return HTTP status code
     */
    int getStatusCode();
}
