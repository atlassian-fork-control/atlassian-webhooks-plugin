package com.atlassian.webhooks;

import javax.annotation.Nonnull;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Optional;

import static java.util.Optional.empty;

/**
 * WebhookScope aims to allow an application utilising this library to segment their webhooks into different scopes.
 * Most applications have an idea of a global event, this for example could be an event that was fired when the
 * application base URL was changed. In this event we are not interested in any webhooks that define themselves to
 * exist at a more granular level.
 * <p>
 * Inversely, if there exists an event that is fired that is specific to a lower level of the application, such as
 * editing a single page/issue/pull request. This event is of importance to that lower scope AND to the global scope.
 * So in that case the WebhookScopes that would be provided would be the lower scope, and the global scope.
 * This would allow for applications to listen for lower level events on a global application level, and at the more
 * granular level as well.
 * <p>
 * Each application must define their own series of scope types and use them as they wish. At its simplest point a
 * scope is just a grouping. So to use these, define a series of groupings at which you'd like to be able to define
 * webhooks.
 *
 * @since 5.0
 */
public interface WebhookScope {

    String TYPE_GLOBAL = "global";

    /**
     * Application-wide scope
     */
    WebhookScope GLOBAL = new WebhookScope() {
        @Nonnull
        @Override
        public Optional<String> getId() {
            return empty();
        }

        @Nonnull
        @Override
        public String getType() {
            return TYPE_GLOBAL;
        }
    };

    /**
     * An associated identifier for this scope. For example, if a scope was PROJECT, the associated identifier might
     * be the specific project ID.
     *
     * @return the identifier associated with the scope, or {@link Optional#empty()} there is none
     */
    @Nonnull
    @Size(max = 255)
    Optional<String> getId();

    /**
     * @return the type of the scope, eg PROJECT
     */
    @NotBlank
    @Size(max = 255)
    String getType();
}
