package com.atlassian.webhooks.event;

import javax.annotation.Nonnull;
import java.util.EventObject;

import static java.util.Objects.requireNonNull;

/**
 * Base class for all events raised in relation to webhooks, such as creation or deletion.
 * <p>
 * Not to be confused with the concept of {@link com.atlassian.webhooks.WebhookEvent}.
 *
 * @since 5.0
 */
public abstract class AbstractWebhookEvent extends EventObject {

    AbstractWebhookEvent(@Nonnull Object source) {
        super(requireNonNull(source, "source"));
    }
}
