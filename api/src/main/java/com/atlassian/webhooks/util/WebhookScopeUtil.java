package com.atlassian.webhooks.util;

import com.atlassian.webhooks.WebhookScope;

public class WebhookScopeUtil {

    private WebhookScopeUtil() {
        throw new UnsupportedOperationException("PropertyUtil is a utility class and should not be instantiated");
    }

    /**
     * Compare webhooks to check for equality. This will only check for values defined in the {@link WebhookScope}
     * interface. This method can be used to compare interfaces that have been defined in different plugins,
     * disregarding the underlying classes. Equality defined on an implementation of WebhookScope may have different
     * semantics due to being provided by the atlassian-webhooks plugin, or the host application. It's safer to compare
     * using this method.
     *
     * @return {@code true} if the two webhook scopes are both equal.
     */
    public static boolean equals(WebhookScope a, WebhookScope b) {
        if (a == b) {
            return true;
        }

        if (a != null && b != null) {
            return a.getId().equals(b.getId()) &&
                    b.getType().equals(b.getType());
        }

        return false;
    }

}
