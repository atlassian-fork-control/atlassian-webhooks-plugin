package com.atlassian.webhooks.history;

import javax.annotation.Nonnull;
import java.time.Duration;
import java.time.Instant;

/**
 * Records a tally of successful, failed and error invocations for a webhook over some period of time.
 *
 * The included period of time is from {@link #getWindowStart()} to the present moment and of
 * {@link #getWindowDuration() a fixed duration}
 *
 * @since 6.1
 */
public interface InvocationCounts {

    /**
     * @return the number of invocations that encountered an error before or during sending of a HTTP request for
     * a webhook invocation. Typically this is due to local network, system or other misadventure. Invocations counted
     * as errors are unlikely to have connected to the recipient.
     */
    int getErrors();

    /**
     * @return the number of invocations in the period that respond over HTTP with a failure status code
     */
    int getFailures();

    /**
     * @return the number of invocations in the period that respond over HTTP with a success status code
     */
    int getSuccesses();

    /**
     * @return the duration of the window for the tallies
     */
    @Nonnull
    Duration getWindowDuration();

    /**
     * @return the start of the window period (the end of the window is always {@link Instant#now() now}) give or
     * take a small relative gap between used to re-calculate the window's tallies
     */
    @Nonnull
    Instant getWindowStart();
}
