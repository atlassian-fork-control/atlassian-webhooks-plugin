package com.atlassian.webhooks.history;

/**
 * Detailed information about the result of the {@link DetailedInvocation invocation} - either an
 * {@link DetailedInvocationError error} or a {@link DetailedInvocationResponse HTTP response}
 *
 * @since 6.1
 */
public interface DetailedInvocationResult extends InvocationResult {
}
