package com.atlassian.webhooks.history;

import com.atlassian.webhooks.request.Method;

import javax.annotation.Nonnull;

/**
 * @since 6.1
 */
public interface InvocationRequest {

    /**
     * @return the HTTP method used when making the request
     */
    @Nonnull
    Method getMethod();

    /**
     * @return the URL the request was made to
     */
    @Nonnull
    String getUrl();
}
