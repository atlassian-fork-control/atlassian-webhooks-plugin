package com.atlassian.webhooks.history;

import javax.annotation.Nonnull;

/**
 * Represents a detailed description of the local error that prevented the invocation's
 * {@link DetailedInvocationRequest HTTP request} from being sent or its {@link DetailedInvocationResponse response}
 * from being processed e.g. a NullPointerException in code, a DNS lookup failure while trying to send to a
 * webhook recipient host, a socket connection error, socket timeout etc
 *
 * @since 6.1
 */
public interface DetailedInvocationError extends DetailedInvocationResult {

    /**
     * @return any additional content of the error that may be useful, e.g. a stack trace
     */
    @Nonnull
    String getContent();

    /**
     * @return the error message of the underlying exception
     */
    @Nonnull
    default String getErrorMessage() {
        return getDescription();
    }
}
