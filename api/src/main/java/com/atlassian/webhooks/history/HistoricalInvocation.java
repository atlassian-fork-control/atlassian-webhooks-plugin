package com.atlassian.webhooks.history;

import com.atlassian.webhooks.WebhookEvent;
import com.atlassian.webhooks.WebhookInvocation;

import javax.annotation.Nonnull;
import java.time.Duration;
import java.time.Instant;

/**
 * A historical record of a {@link WebhookInvocation webhook invocation}
 *
 * @since 6.1
 */
public interface HistoricalInvocation {

    /**
     * @return the total duration of the invocation from the {@link #getStart() start} of the HTTP request to the
     * {@link #getFinish() end} of the HTTP response or processing of the error if one is encountered
     */
    @Nonnull
    Duration getDuration();

    /**
     * @return the {@link WebhookEvent event} that generated the invocation
     */
    @Nonnull
    WebhookEvent getEvent();

    /**
     * @return when the invocation finished. Straight after the response is received from the webhook or an error is
     * encountered
     */
    @Nonnull
    Instant getFinish();

    /**
     * @return the invocation ID that uniquely identifies the original invocation
     */
    @Nonnull
    String getId();

    /**
     * @return the details of the invocation's HTTP request
     */
    @Nonnull
    InvocationRequest getRequest();

    /**
     * @return the result of the invocation's request
     */
    @Nonnull
    InvocationResult getResult();

    /**
     * @return when the invocation began. Just prior to when the HTTP request was made
     */
    @Nonnull
    Instant getStart();
}
