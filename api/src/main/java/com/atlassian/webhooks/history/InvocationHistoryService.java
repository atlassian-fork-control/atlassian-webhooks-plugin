package com.atlassian.webhooks.history;

import com.atlassian.webhooks.NoSuchWebhookException;
import com.atlassian.webhooks.Webhook;
import com.atlassian.webhooks.WebhookEvent;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;

/**
 * Provides various historical information about {@link com.atlassian.webhooks.WebhookInvocation invocations}
 * for {@link Webhook webhooks}.
 *
 * @since 6.1
 */
public interface InvocationHistoryService {

    /**
     * @return a historical summary of invocations for the supplied {@link Webhook webhook} restricted to only those
     *         invoked for the supplied {@link WebhookEvent event} if non-null
     * @throws NoSuchWebhookException if history is requested for a webhook that does not exist
     */
    @Nonnull
    InvocationHistory get(@Nonnull InvocationHistoryRequest request);

    /**
     * @return historical summaries of invocations for all {@link WebhookEvent events} for the supplied
     *         {@link Webhook webhook}
     * @throws NoSuchWebhookException if history is requested for a webhook that does not exist
     */
    @Nonnull
    Map<WebhookEvent, InvocationHistory> getByEvent(@Nonnull InvocationHistoryByEventRequest request);

    /**
     * @param webhookIds the IDs of the webhooks for which the invocation history is requested
     * @return a {@link Map map} of {@link Webhook#getId() webhook IDs} to the corresponding
     *         {@link InvocationHistory invocation history}
     */
    @Nonnull
    Map<Integer, InvocationHistory> getByWebhook(Collection<Integer> webhookIds);

    /**
     * @return the most recent {@link DetailedInvocation webhook invocation} matching the specified
     *         {@link InvocationOutcome result} and other requested criteria or
     *         {@link Optional#empty() an empty result} if there have been no such invocations
     * @throws NoSuchWebhookException if invocation data is requested for a webhook that does not exist
     */
    @Nonnull
    Optional<DetailedInvocation> getLatestInvocation(@Nonnull HistoricalInvocationRequest request);
}
