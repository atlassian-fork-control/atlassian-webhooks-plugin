package com.atlassian.webhooks.history;

/**
 * Specifies the possible outcomes of a webhook invocation
 *
 * @since 6.1
 */
public enum InvocationOutcome {

    /**
     * Indicates an invocation resulted in a failure to make a HTTP request or an unexpected error processing the response
     */
    ERROR,
    /**
     * Indicates an invocation resulted in a HTTP request being made and a response being received with a code indicating
     * delivery failure e.g. 400, 500
     */
    FAILURE,
    /**
     * Indicates an invocation resulted in a HTTP request being made and a response being received with a status
     * in the 200 range
     */
    SUCCESS
}
