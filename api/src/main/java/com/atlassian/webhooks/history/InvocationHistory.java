package com.atlassian.webhooks.history;

import com.atlassian.webhooks.WebhookInvocation;

import javax.annotation.Nonnull;
import java.util.Optional;

/**
 * Provides a historical, summarising view on past invocations of a {@link WebhookInvocation}.
 *
 * @since 6.1
 */
public interface InvocationHistory {

    /**
     * @return the last webhook invocation that encountered an error while sending a HTTP request
     * or {@link Optional#empty() an empty result} if there has been no such invocation
     */
    @Nonnull
    Optional<HistoricalInvocation> getLastError();

    /**
     * @return the last webhook invocation to receive a HTTP response in the 500 range or
     * {@link Optional#empty() an empty result} if there has been no such invocation
     */
    @Nonnull
    Optional<HistoricalInvocation> getLastFailure();

    /**
     * @return the last webhook invocation to complete successfully or {@link Optional#empty() an empty result} if
     * there has been no such invocation
     */
    @Nonnull
    Optional<HistoricalInvocation> getLastSuccess();

    /**
     * @return the {@link InvocationCounts invocation counts} for the relevant interval
     */
    @Nonnull
    InvocationCounts getCounts();
}
