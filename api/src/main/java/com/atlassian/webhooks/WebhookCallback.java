package com.atlassian.webhooks;

import com.atlassian.webhooks.request.WebhookHttpRequest;
import com.atlassian.webhooks.request.WebhookHttpResponse;

import javax.annotation.Nonnull;

/**
 * A callback that can be registered to retrieve the results of a webhook invocation
 *
 * @since 5.0
 */
public interface WebhookCallback {

    /**
     * Called when an Exception was raised during the processing of the webhook invocation
     *
     * @param request the request that was attempted
     * @param error the exception that was raised
     * @param invocation the invocation that caused this HTTP request
     */
    void onError(WebhookHttpRequest request, @Nonnull Throwable error, @Nonnull WebhookInvocation invocation);

    /**
     * A callback method that is invoked when the webhook invocation returns any non-2xx response
     *
     * @param request the request that was attempted
     * @param response the response from that attempt
     * @param invocation the invocation that caused this HTTP request
     */
    void onFailure(@Nonnull WebhookHttpRequest request, @Nonnull WebhookHttpResponse response,
                   @Nonnull WebhookInvocation invocation);

    /**
     * A callback method that is invoked when the webhook invocation receives a 2xx response
     *
     * @param request the request that was attempted
     * @param response the response from that attempt
     * @param invocation the invocation that caused this HTTP request
     */
    void onSuccess(@Nonnull WebhookHttpRequest request, @Nonnull WebhookHttpResponse response,
                   @Nonnull WebhookInvocation invocation);
}
