package com.atlassian.webhooks;

/**
 * Statistics about publishing and dispatching of webhooks
 */
public interface WebhookStatistics {

    /**
     * @return the total number of webhooks that were dispatched to listeners. If 1 webhook is published to
     *         10 listeners, this results in 10 dispatches. The dispatched count is equal to
     *         {@link #getDispatchSuccessCount()} + {@link #getDispatchFailureCount()} +
     *         {@link #getDispatchErrorCount()}. Note that {@link #getDispatchRejectedCount()} is not included in
     *         this count.
     *
     */
    long getDispatchedCount();

    /**
     * @return the total number of webhooks that were {@link WebhookService#publish published}
     */
    long getPublishedCount();

    /**
     * @return the number of dispatches to a listener that failed because of errors such as socket timeouts or
     *         connection errors
     */
    long getDispatchErrorCount();

    /**
     * @return the number of dispatches to a listener that failed because the listener returned a non-success
     *         status code (e.g. 404, 500)
     */
    long getDispatchFailureCount();

    /**
     * @return the number of dispatches that were rejected because too many dispatches were already in flight,
     *         or because dispatches to a webhook listener had failed too often in a row
     */
    long getDispatchRejectedCount();

    /**
     * @return the number of dispatches that succeeded (where the listener returned a 2xx status code)
     */
    long getDispatchSuccessCount();
}
