package com.atlassian.webhooks;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * Exception that is thrown when a webhook cannot be dispatched
 *
 * @since 5.0
 */
public class DispatchFailedException extends RuntimeException {

    static final long serialVersionUID = 1L;

    private final WebhookInvocation invocation;

    public DispatchFailedException(@Nonnull WebhookInvocation invocation, String message) {
        super(message);

        this.invocation = requireNonNull(invocation, "invocation");
    }

    @Nonnull
    public WebhookInvocation getInvocation() {
        return invocation;
    }
}
