package com.atlassian.webhooks;

import com.atlassian.webhooks.request.WebhookHttpRequest;

import javax.annotation.Nonnull;
import java.util.Optional;

/**
 * A specific invocation of a webhook for a single payload. An invocation is triggered after a call to
 * {@link WebhookService#publish(WebhookPublishRequest)}
 *
 * @since 5.0
 */
public interface WebhookInvocation {

    /**
     * Retrieve a unique identifier for this specific invocation of a webhook. This should be used to differentiate
     * between different invocations of the same webhook.
     *
     * @return a unique identifier of the invocation
     */
    @Nonnull
    String getId();

    /**
     * @return the event for which the webhook was invoked
     */
    @Nonnull
    WebhookEvent getEvent();

    /**
     * Retrieves the payload to be sent along with the webhook. This could be the event that caused the webhook to be
     * fired. By default, this will be used to construct the body of the HTTP request.
     *
     * @return the payload for sending in the webhook
     */
    @Nonnull
    Optional<Object> getPayload();

    /**
     * Builder for the HTTP request that is about to be sent to the {@link #getWebhook() webhook}.
     * <p>
     * By default, the values represent what is provided in the associated {@link Webhook} from {@link #getWebhook()}.
     * Any details that are not specified in the webhook, such as headers, are left empty until otherwise changed.
     *
     * @return the current HTTP context
     */
    @Nonnull
    WebhookHttpRequest.Builder getRequestBuilder();

    /**
     * The webhook that has been triggered. This contains details about the context, url, and other factors that can
     * be used in constructing the HTTP request, or in filtering invocations.
     *
     * @return the triggered webhook
     */
    @Nonnull
    Webhook getWebhook();

    /**
     * Used to register a callback that will be triggered once the webhook has completed, allowing the caller to capture
     * details about the results of the webhook.
     *
     * @param callback callback to trigger on completion of webhook
     */
    void registerCallback(@Nonnull WebhookCallback callback);
}
