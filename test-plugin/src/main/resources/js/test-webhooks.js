(function ($) {
    webhooks.addFilter("section-with-filter", function () {
        return $("#webhook-filter").val();
    }, function (value) {
        $("#webhook-filter-display").html(value);
        $("#webhook-filter").val(value);
    });

    webhooks.setDateFormatter(function (date) {
        return date.toLocaleDateString() + " " + date.toLocaleTimeString() + " (date formatted by Plug-in)";
    });

    webhooks.addFilter("section-with-cool-filter", function() {
        return $("input:radio[name=coolEvents]:checked").attr("id")
    }, function(value) {
        $("#" + value).prop("checked", true)
        $("#coolEventsDisplay").html(value);
    });
})(AJS.$);
