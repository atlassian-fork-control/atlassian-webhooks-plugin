package com.atlassian.webhooks.plugin.legacy;

public class LegacyProviderEvent
{
    private final String somePayload;
    private final boolean shouldBeMatched;

    public LegacyProviderEvent(final String somePayload, final boolean shouldBeMatched) {this.somePayload = somePayload;
        this.shouldBeMatched = shouldBeMatched;
    }

    public boolean isShouldBeMatched()
    {
        return shouldBeMatched;
    }

    public String getSomePayload()
    {
        return somePayload;
    }
}
