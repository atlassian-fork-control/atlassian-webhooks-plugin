package com.atlassian.webhooks.plugin.test.servlet;

public class PropertiesServlet extends AbstractWebHookListeningServlet {
    public static final String QUEUE_NAME = "properties-queue";

    public PropertiesServlet(final WebHooksQueues webHooksQueues) {
        super(webHooksQueues);
    }

    @Override
    String queueName() {
        return QUEUE_NAME;
    }
}
