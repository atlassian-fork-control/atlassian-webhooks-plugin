package com.atlassian.webhooks.plugin.test.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class AbstractWebHookListeningServlet extends HttpServlet
{
    private final WebHooksQueues webHooksQueues;

    protected AbstractWebHookListeningServlet(final WebHooksQueues webHooksQueues)
    {
        this.webHooksQueues = webHooksQueues;
    }

    @Override
    protected final void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException
    {
        final ReceivedWebHook hook = ReceivedWebHook.fromServletRequest(req);

        webHooksQueues.enqueue(queueName(), hook);
    }

    abstract String queueName();
}
