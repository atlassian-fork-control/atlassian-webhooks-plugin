package com.atlassian.webhooks.plugin.test.servlet;

public class ExcludedEventBodyServlet extends AbstractWebHookListeningServlet
{

    public static final String QUEUE_NAME = "excludeBodyQueue";

    protected ExcludedEventBodyServlet(WebHooksQueues webHooksQueues)
    {
        super(webHooksQueues);
    }

    @Override
    String queueName()
    {
        return QUEUE_NAME;
    }
}
