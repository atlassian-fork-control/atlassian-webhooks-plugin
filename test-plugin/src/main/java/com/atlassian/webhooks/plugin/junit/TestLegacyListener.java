package com.atlassian.webhooks.plugin.junit;

import com.atlassian.webhooks.plugin.event.TestEvent;
import com.atlassian.webhooks.plugin.legacy.LegacyEventWithPluginAndModuleKey;
import com.atlassian.webhooks.plugin.legacy.LegacyMatcherEvent;
import com.atlassian.webhooks.plugin.test.servlet.ReceivedWebHook;
import io.atlassian.fugue.Option;
import org.junit.Test;

import static com.atlassian.webhooks.plugin.legacy.LegacyListenerRegistrator.MODULE_KEY;
import static com.atlassian.webhooks.plugin.legacy.LegacyListenerRegistrator.PLUGIN_KEY_FOR_COMPONENTS;
import static com.atlassian.webhooks.plugin.legacy.LegacyListenerRegistrator.PLUGIN_KEY_IN_PARAMS;
import static com.atlassian.webhooks.plugin.test.servlet.LegacyListenerServlet.QUEUE_NAME;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public final class TestLegacyListener extends WebHookUnitTest
{
    @Test
    public void simpleLegacyMatcherWorksForLegacyListeners()
    {
        Option<ReceivedWebHook> webhook = raiseEventAndReceiveWebhook(new LegacyMatcherEvent(true));
        assertTrue(webhook.isDefined());

        webhook = raiseEventAndReceiveWebhook(new LegacyMatcherEvent(false));
        assertTrue(webhook.isEmpty());
    }

    @Test
    public void pluginKeyAndModuleKeyArePassedOnToTheLegacyMatchers()
    {
        Option<ReceivedWebHook> webhook = raiseEventAndReceiveWebhook(new LegacyEventWithPluginAndModuleKey(PLUGIN_KEY_IN_PARAMS, MODULE_KEY));
        assertTrue(webhook.isDefined());

        webhook = raiseEventAndReceiveWebhook(new LegacyEventWithPluginAndModuleKey("some other plugin key", "some other module key"));
        assertTrue(webhook.isEmpty());
    }

    @Test
    public void properPluginKeyIsPassedOnToTheRequestSignerForLegacyWebHookAndListener()
    {
        Option<ReceivedWebHook> webhook = raiseEventAndReceiveWebhook(new LegacyMatcherEvent(true));
        assertEquals(PLUGIN_KEY_FOR_COMPONENTS, webhook.get().getHeader("plugin-key"));
    }

    @Test
    public void legacyListenersCanRegisterToNewWebHooks()
    {
        Option<ReceivedWebHook> webhook = raiseEventAndReceiveWebhook(new TestEvent("payload"));
        assertTrue(webhook.get().getBody().contains("payload"));
        assertEquals(PLUGIN_KEY_FOR_COMPONENTS, webhook.get().getHeader("plugin-key"));
    }

    @Test
    public void properPluginKeyIsPassedOnToTheRequestSignerForLegacyListenerAndNewWebHook()
    {
        Option<ReceivedWebHook> webhook = raiseEventAndReceiveWebhook(new TestEvent("payload"));
        assertEquals(PLUGIN_KEY_FOR_COMPONENTS, webhook.get().getHeader("plugin-key"));
    }

    private Option<ReceivedWebHook> raiseEventAndReceiveWebhook(Object event)
    {
        return raiseEventAndReceiveWebHook(event, QUEUE_NAME);
    }
}
