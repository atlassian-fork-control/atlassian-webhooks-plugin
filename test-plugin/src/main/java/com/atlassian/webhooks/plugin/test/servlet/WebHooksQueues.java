package com.atlassian.webhooks.plugin.test.servlet;

import io.atlassian.fugue.Option;

import java.util.Map;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class WebHooksQueues
{
    private final Map<String, BlockingDeque<ReceivedWebHook>> queues = new ConcurrentHashMap<String, BlockingDeque<ReceivedWebHook>>();
    private final Lock lock = new ReentrantLock();

    public void enqueue(final String queueName, final ReceivedWebHook webHook)
    {
        lock.lock();

        final BlockingDeque<ReceivedWebHook> receivedWebHooks;
        try
        {
            receivedWebHooks = getQueue(queueName);
            receivedWebHooks.push(webHook);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        lock.unlock();
    }

    public Option<ReceivedWebHook> getAndWait(final String queueName)
    {
        try
        {
            return Option.option(getQueue(queueName).poll(3, TimeUnit.SECONDS));
        }
        catch (InterruptedException e)
        {
            return Option.none();
        }
    }

    public BlockingDeque<ReceivedWebHook> getQueue(final String queue) throws InterruptedException
    {
        try
        {
            lock.lockInterruptibly();

            final BlockingDeque<ReceivedWebHook> receivedWebHooks = queues.get(queue);

            if (receivedWebHooks == null)
            {
                final LinkedBlockingDeque<ReceivedWebHook> hooks = new LinkedBlockingDeque<ReceivedWebHook>();
                queues.put(queue, hooks);
                return hooks;
            }
            else
            {
                return receivedWebHooks;
            }
        }
        finally
        {
            lock.unlock();
        }
    }

    public void cleanAll()
    {
        for (Map.Entry<String, BlockingDeque<ReceivedWebHook>> entry : queues.entrySet())
        {
            entry.getValue().clear();
        }
    }
}
