package com.atlassian.webhooks.plugin.test.servlet;

public class PluginEnabledWebHookServlet extends AbstractWebHookListeningServlet
{
    public static final String QUEUE_NAME = "plugin-enabled-queue";

    protected PluginEnabledWebHookServlet(final WebHooksQueues webHooksQueues)
    {
        super(webHooksQueues);
    }

    @Override
    String queueName()
    {
        return QUEUE_NAME;
    }
}
