package com.atlassian.webhooks.plugin.junit;

import com.atlassian.sal.api.UrlMode;
import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.plugin.test.ServiceAccessor;
import com.atlassian.webhooks.plugin.test.servlet.ReceivedWebHook;
import io.atlassian.fugue.Option;
import org.junit.Before;

public abstract class WebHookUnitTest
{
    @Before
    public void setUp() throws Exception
    {
        deleteAllRegisteredListeners();
        cleanUpAllQueues();
    }

    private void deleteAllRegisteredListeners()
    {
        for (PersistentWebHookListener listener : ServiceAccessor.getWebHookListenerService().getAllWebHookListeners())
        {
            ServiceAccessor.getWebHookListenerService().deleteWebHookListener(listener.getId().get());
        }
    }

    private void cleanUpAllQueues()
    {
        ServiceAccessor.getWebHooksQueues().cleanAll();
    }

    protected final String urlToServlet(String path)
    {
        return ServiceAccessor.getApplicationProperties().getBaseUrl(UrlMode.CANONICAL) + "/plugins/servlet/test/" + path;
    }

    protected final Option<ReceivedWebHook> raiseEventAndReceiveWebHook(Object event, String queueName)
    {
        ServiceAccessor.getEventPublisher().publish(event);
        return ServiceAccessor.getWebHooksQueues().getAndWait(queueName);
    }
}
