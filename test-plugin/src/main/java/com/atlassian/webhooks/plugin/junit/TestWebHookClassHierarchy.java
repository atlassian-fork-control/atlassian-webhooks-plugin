package com.atlassian.webhooks.plugin.junit;

import com.atlassian.webhooks.plugin.event.TestEvent;
import com.atlassian.webhooks.plugin.test.ServiceAccessor;
import com.atlassian.webhooks.plugin.test.servlet.ReceivedWebHook;
import com.atlassian.webhooks.plugin.test.servlet.TestEventServlet;
import io.atlassian.fugue.Option;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class TestWebHookClassHierarchy extends WebHookUnitTest
{
    @Test
    public void handlesSubclassesOfRegisteredEvents() throws InterruptedException
    {
        final String eventValue = "Event value";
        ServiceAccessor.getEventPublisher().publish(new ExtTestEvent(eventValue));

        final Option<ReceivedWebHook> webhookForSubclass = ServiceAccessor.getWebHooksQueues().getAndWait(TestEventServlet.QUEUE_NAME);

        assertTrue(webhookForSubclass.isDefined());
        assertTrue(webhookForSubclass.get().getBody().contains(eventValue));
    }

    private static class ExtTestEvent extends TestEvent
    {
        public ExtTestEvent(final String value)
        {
            super(value);
        }
    }
}
