package com.atlassian.webhooks.plugin.legacy;

import com.atlassian.webhooks.spi.provider.ModuleDescriptorWebHookListenerRegistry;
import com.atlassian.webhooks.spi.provider.PluginModuleListenerParameters;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;

import java.net.URI;
import java.util.Collections;

public final class LegacyListenerRegistrator
{
    public static final String PLUGIN_KEY_IN_PARAMS = "pluginKey";
    public static final String PLUGIN_KEY_FOR_COMPONENTS = "legacy_plugin";
    public static final String MODULE_KEY = "moduleKey";

    private final ModuleDescriptorWebHookListenerRegistry registry;

    public LegacyListenerRegistrator(final ModuleDescriptorWebHookListenerRegistry registry)
    {
        this.registry = Preconditions.checkNotNull(registry);
        provideListeners();
    }

    private void provideListeners()
    {
        registry.register("legacy_event_matcher", PLUGIN_KEY_FOR_COMPONENTS, URI.create("/plugins/servlet/test/legacy-listener"),
                new PluginModuleListenerParameters(PLUGIN_KEY_IN_PARAMS, Optional.of(MODULE_KEY),
                        ImmutableMap.<String, Object>of("shouldMatch", Boolean.TRUE), "legacy_provider_event"));

        registry.register("legacy_plugin_key_event_matcher", PLUGIN_KEY_FOR_COMPONENTS, URI.create("/plugins/servlet/test/legacy-listener"),
                new PluginModuleListenerParameters(PLUGIN_KEY_IN_PARAMS, Optional.of(MODULE_KEY),
                        ImmutableMap.<String, Object>of("shouldMatch", Boolean.TRUE), "legacy_plugin_key_event_matcher"));

        registry.register("test_event", PLUGIN_KEY_FOR_COMPONENTS, URI.create("/plugins/servlet/test/legacy-listener"),
                new PluginModuleListenerParameters(PLUGIN_KEY_IN_PARAMS, Optional.of(MODULE_KEY),
                        Collections.<String, Object>emptyMap(), "test_event"));
    }
}
