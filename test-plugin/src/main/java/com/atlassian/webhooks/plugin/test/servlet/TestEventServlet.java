package com.atlassian.webhooks.plugin.test.servlet;

public class TestEventServlet extends AbstractWebHookListeningServlet
{
    public static final String QUEUE_NAME = "test-event-queue";

    protected TestEventServlet(final WebHooksQueues webHooksQueues)
    {
        super(webHooksQueues);
    }

    @Override
    String queueName()
    {
        return QUEUE_NAME;
    }
}
