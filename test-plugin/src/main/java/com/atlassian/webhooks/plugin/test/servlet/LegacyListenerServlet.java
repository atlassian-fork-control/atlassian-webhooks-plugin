package com.atlassian.webhooks.plugin.test.servlet;

public final class LegacyListenerServlet extends AbstractWebHookListeningServlet
{
    public static final String QUEUE_NAME = "legacyListenerQueue";

    public LegacyListenerServlet(final WebHooksQueues webHooksQueues)
    {
        super(webHooksQueues);
    }

    @Override
    public String queueName()
    {
        return QUEUE_NAME;
    }
}
