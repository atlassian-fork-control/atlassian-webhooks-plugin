package com.atlassian.webhooks.plugin.test.servlet;

public class UriSubstitutionServlet extends AbstractWebHookListeningServlet
{
    public static final String QUEUE_NAME = "uri-substitution-queue";

    public UriSubstitutionServlet(final WebHooksQueues webHooksQueues)
    {
        super(webHooksQueues);
    }

    @Override
    String queueName()
    {
        return QUEUE_NAME;
    }


}
