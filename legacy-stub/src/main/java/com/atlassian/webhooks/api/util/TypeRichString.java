package com.atlassian.webhooks.api.util;

import com.google.common.base.Function;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.io.Serializable;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A simple wrapper over String values for type-rich programming.
 */
@Deprecated
@Immutable
public abstract class TypeRichString implements Serializable {
    public static final Function<TypeRichString, String> GET_VALUE = new Function<TypeRichString, String>() {
        @Override
        public String apply(@Nullable final TypeRichString input) {
            return input != null ? input.getValue() : null;
        }
    };

    private final String value;

    public TypeRichString(String value) {
        this.value = checkNotNull(value);
    }

    public final String getValue() {
        return value;
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && this.getClass().equals(obj.getClass()) && value.equals(TypeRichString.class.cast(obj).getValue());
    }

    @Override
    public String toString() {
        return value;
    }


}
