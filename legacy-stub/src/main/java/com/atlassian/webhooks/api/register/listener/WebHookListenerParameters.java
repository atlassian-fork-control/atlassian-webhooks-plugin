package com.atlassian.webhooks.api.register.listener;

import com.google.common.base.Objects;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import javax.annotation.concurrent.Immutable;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

@Deprecated
@Immutable
public class WebHookListenerParameters {
    private final String targetUrl;
    private final boolean excludeBody;
    private final String filter;
    private final List<String> propertyKeys;

    public WebHookListenerParameters(String targetUrl, String filter, boolean excludeBody) {
        this(targetUrl, filter, excludeBody, Lists.newArrayList());
    }

    public WebHookListenerParameters(String targetUrl, String filter, boolean excludeBody, Iterable<String> propertyKeys) {
        this.excludeBody = excludeBody;
        this.targetUrl = checkNotNull(targetUrl);
        this.filter = Strings.nullToEmpty(filter);
        this.propertyKeys = ImmutableList.copyOf(checkNotNull(propertyKeys));
    }

    public String getUrl() {
        return targetUrl;
    }

    public String getFilter() {
        return filter;
    }

    public boolean isExcludeBody() {
        return excludeBody;
    }

    public List<String> getPropertyKeys() {
        return propertyKeys;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(targetUrl, excludeBody, filter, propertyKeys);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final WebHookListenerParameters other = (WebHookListenerParameters) obj;
        return Objects.equal(this.targetUrl, other.targetUrl)
                && Objects.equal(this.excludeBody, other.excludeBody)
                && Objects.equal(this.filter, other.filter)
                && Objects.equal(this.propertyKeys, other.propertyKeys);
    }

    public static final class Names {
        public static final String EXCLUDE_BODY = "excludeBody";
        public static final String FILTER = "filter";
        public static final String PROPERTY_KEY = "propertyKey";
    }
}
