package com.atlassian.webhooks.api.events;

import com.atlassian.webhooks.api.register.listener.WebHookListenerRegistrationDetails;
import io.atlassian.fugue.Option;

import static io.atlassian.fugue.Option.option;

@Deprecated
public abstract class AbstractWebHookPublishResultEvent {
    private final String webHookId;
    private final String pluginKey;

    protected AbstractWebHookPublishResultEvent(String webHookId, WebHookListenerRegistrationDetails registrationDetails) {
        this.webHookId = webHookId;
        this.pluginKey = registrationDetails.getModuleDescriptorDetails().isDefined()
                ? registrationDetails.getModuleDescriptorDetails().get().getPluginKey()
                : null;
    }

    public final String getWebHookId() {
        return webHookId;
    }

    public Option<String> getPluginKey() {
        return option(pluginKey);
    }

}
