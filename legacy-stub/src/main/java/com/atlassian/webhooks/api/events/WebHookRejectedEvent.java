package com.atlassian.webhooks.api.events;

import com.atlassian.webhooks.api.register.listener.WebHookListenerRegistrationDetails;

/**
 * Fired when the web hook publishing queue is full and the event will be discarded
 */
@Deprecated
public final class WebHookRejectedEvent extends AbstractWebHookPublishResultEvent {
    private final String rejectionMessage;

    public WebHookRejectedEvent(String webHookId, WebHookListenerRegistrationDetails registrationDetails, String rejectionMessage) {
        super(webHookId, registrationDetails);
        this.rejectionMessage = rejectionMessage;
    }

    public String getRejectionMessage() {
        return rejectionMessage;
    }
}
