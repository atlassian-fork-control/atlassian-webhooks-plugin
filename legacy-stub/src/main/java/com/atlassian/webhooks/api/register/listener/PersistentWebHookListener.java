package com.atlassian.webhooks.api.register.listener;

import com.atlassian.webhooks.api.util.Filter;
import com.atlassian.webhooks.api.util.SectionKey;
import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import io.atlassian.fugue.Option;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import static com.google.common.base.Strings.nullToEmpty;

/**
 * Webhook listener that can be saved in a persistent store.
 */
@Deprecated
@Immutable
public class PersistentWebHookListener implements Serializable {
    private final Option<Integer> listenerId;
    private final String listenerName;
    private final String description;
    private final ImmutableSet<String> webHookIds;
    private final String url;
    private final boolean enabled;
    private final boolean excludeBody;
    private final ImmutableMap<SectionKey, Filter> filters;
    private final Date lastUpdated;
    private final String lastUpdatedByUser;

    private PersistentWebHookListener(Option<Integer> listenerId, String listenerName, String description,
                                      Set<String> webHookIds, String url, boolean enabled, boolean excludeBody,
                                      Map<SectionKey, Filter> filters, Date lastUpdated, String lastUpdatedByUser) {
        this.listenerId = listenerId;
        this.listenerName = nullToEmpty(listenerName);
        this.description = description;
        this.webHookIds = ImmutableSet.copyOf(webHookIds);
        this.url = nullToEmpty(url);
        this.enabled = enabled;
        this.excludeBody = excludeBody;
        this.filters = ImmutableMap.copyOf(filters);
        this.lastUpdated = MoreObjects.firstNonNull(lastUpdated, new Date());
        this.lastUpdatedByUser = nullToEmpty(lastUpdatedByUser);
    }

    public static Builder newlyCreated() {
        return new Builder(null);
    }

    public static Builder existing(Integer id) {
        return new Builder(id);
    }

    public Option<Integer> getId() {
        return listenerId;
    }

    public String getName() {
        return listenerName;
    }

    public String getDescription() {
        return description;
    }

    public Set<String> getEvents() {
        return webHookIds;
    }

    public String getUrl() {
        return url;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public boolean isExcludeBody() {
        return excludeBody;
    }

    public Map<SectionKey, Filter> getFilters() {
        return filters;
    }

    public Date getLastUpdated() {
        return new Date(lastUpdated.getTime()); // defensive copy
    }

    public String getLastUpdatedByUser() {
        return lastUpdatedByUser;
    }

    /**
     * Returns a filter for the specified section, or empty string if the filter is not defined.
     *
     * @param sectionKey section for which we want the filter
     * @return filter or empty string if not found or if the section key is null
     */
    public String getFilterFor(@Nullable SectionKey sectionKey) {
        if (sectionKey == null) {
            return "";
        }
        return MoreObjects.firstNonNull(filters.get(sectionKey), new Filter("")).getValue();
    }

    public static class Builder {
        private final Integer listenerId;
        private final Set<String> webHookIds = Sets.newHashSet();
        private final Map<SectionKey, Filter> filters = Maps.newHashMap();
        private String listenerName;
        private String description;
        private String url;
        private boolean enabled;
        private boolean excludeBody;
        private Date lastUpdated = new Date();
        private String lastUpdatedByUser;

        private Builder(Integer listenerId) {
            this.listenerId = listenerId;
        }

        public Builder setListenerName(String listenerName) {
            this.listenerName = listenerName;
            return this;
        }

        public Builder setDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder addWebHookId(String webhookId) {
            this.webHookIds.add(webhookId);
            return this;
        }

        public Builder addWebHookIds(Collection<String> webHookIds) {
            this.webHookIds.addAll(webHookIds);
            return this;
        }

        public Builder setUrl(String url) {
            this.url = url;
            return this;
        }

        public Builder setEnabled(boolean enabled) {
            this.enabled = enabled;
            return this;
        }

        public Builder setExcludeBody(boolean excludeBody) {
            this.excludeBody = excludeBody;
            return this;
        }

        public Builder addFilter(String section, String filter) {
            filters.put(new SectionKey(section), new Filter(filter));
            return this;
        }

        public Builder addFilters(Map<String, String> filters) {
            for (Map.Entry<String, String> entry : filters.entrySet()) {
                this.filters.put(new SectionKey(entry.getKey()), new Filter(entry.getValue()));
            }
            return this;
        }

        public Builder addFiltersTypeRich(Map<SectionKey, Filter> filters) {
            this.filters.putAll(filters);
            return this;
        }

        public Builder setLastUpdated(Date lastUpdated) {
            this.lastUpdated = lastUpdated;
            return this;
        }

        public Builder setLastUpdatedByUser(String lastUpdatedByUser) {
            this.lastUpdatedByUser = lastUpdatedByUser;
            return this;
        }

        public PersistentWebHookListener build() {
            return new PersistentWebHookListener(Option.option(listenerId), listenerName, description, webHookIds, url,
                    enabled, excludeBody, filters, lastUpdated, lastUpdatedByUser);
        }

    }
}
