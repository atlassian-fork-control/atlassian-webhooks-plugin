package com.atlassian.webhooks.api.util;

import com.atlassian.httpclient.api.HttpStatus;
import com.atlassian.sal.api.message.Message;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import javax.annotation.concurrent.Immutable;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * This is an immutable version of error message collection.
 */
@Deprecated
@Immutable
public final class MessageCollection {
    private static final MessageCollection EMPTY_MESSAGE_COLLECTION = new MessageCollection(ImmutableList.<Message>of(), ImmutableSet.<Reason>of());

    private final List<Message> messages;
    private final Set<Reason> reasons;

    private MessageCollection(List<Message> messages, Set<Reason> reasons) {
        this.messages = messages;
        this.reasons = reasons;
    }

    public static MessageCollection empty() {
        return EMPTY_MESSAGE_COLLECTION;
    }

    public static MessageCollection of(String message, Reason reason) {
        return builder().addMessage(message, reason).build();
    }

    public static Builder builder() {
        return new Builder();
    }

    public boolean isEmpty() {
        return messages.isEmpty();
    }

    public List<Message> getMessages() {
        return messages;
    }

    public Set<Reason> getReasons() {
        return reasons;
    }

    public static enum Reason {
        NOT_FOUND(HttpStatus.NOT_FOUND),
        NOT_LOGGED_IN(HttpStatus.UNAUTHORIZED),
        FORBIDDEN(HttpStatus.FORBIDDEN),
        VALIDATION_FAILED(HttpStatus.BAD_REQUEST),
        CONFLICT(HttpStatus.CONFLICT);

        private HttpStatus httpStatusCode;

        private Reason(HttpStatus httpStatusCode) {
            this.httpStatusCode = httpStatusCode;
        }

        public static Reason getWorstReason(final Collection<Reason> reasons) {
            if (reasons.contains(NOT_LOGGED_IN)) {
                return NOT_LOGGED_IN;
            }
            if (reasons.contains(FORBIDDEN)) {
                return FORBIDDEN;
            }
            if (reasons.contains(NOT_FOUND)) {
                return NOT_FOUND;
            }
            if (reasons.contains(VALIDATION_FAILED)) {
                return VALIDATION_FAILED;
            }
            if (reasons.contains(CONFLICT)) {
                return CONFLICT;
            }
            return null;
        }

        public int getHttpStatusCode() {
            return httpStatusCode.code;
        }
    }

    public static class Builder {
        private final List<Message> messages = Lists.newArrayList();
        private final Set<Reason> reasons = Sets.newHashSet();

        public Builder addMessage(final String message, final Reason reason) {
            return addMessage(new ErrorMessage(message), reason);
        }

        public Builder addMessage(final Message errorMessage, final Reason reason) {
            return addAll(Lists.newArrayList(errorMessage), Sets.newHashSet(reason));
        }

        public Builder addAll(final List<Message> errorMessages, Set<Reason> reasons) {
            this.messages.addAll(errorMessages);
            this.reasons.addAll(reasons);
            return this;
        }

        public Builder addAll(final MessageCollection msgCollection) {
            this.messages.addAll(msgCollection.getMessages());
            this.reasons.addAll(msgCollection.getReasons());
            return this;
        }

        public MessageCollection build() {
            return new MessageCollection(ImmutableList.copyOf(messages), ImmutableSet.copyOf(reasons));
        }
    }
}
