package com.atlassian.webhooks.api.register.listener;

import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;
import java.util.Collections;
import java.util.List;

/**
 * Web hook listener that was registered for a particular webhook event.
 * <p>
 * webhook listeners can come either from module descriptors or persistent stores.
 * In the first case they are registered for one particular event and don't actually
 * differ in representation from this class.
 * <p>
 * The story with persistent webhook listeners is different. They can be registered
 * for a whole set of events and they may have many filters (one for each webhook section).
 * This class is such a webhook listener representation narrowed down to the context of
 * webhook event that is currently in processing.
 */
@Deprecated
@Immutable
public final class WebHookListener {
    private final WebHookListenerRegistrationDetails registrationDetails;
    private final WebHookListenerParameters params;

    private WebHookListener(WebHookListenerRegistrationDetails registrationDetails, WebHookListenerParameters params) {
        this.registrationDetails = registrationDetails;
        this.params = params;
    }

    public static Builder fromModuleDescriptor(@Nonnull String pluginKey) {
        return new Builder(WebHookListenerRegistrationDetails.moduleDescriptor(pluginKey));
    }

    public static Builder fromPersistentStore(Integer id, String listenerName) {
        return new Builder(WebHookListenerRegistrationDetails.persistentStore(id, listenerName, Optional.<String>absent()));
    }

    public static Builder fromPersistentStore(Integer id, String listenerName, Optional<String> lastUpdatedUserKey) {
        return new Builder(WebHookListenerRegistrationDetails.persistentStore(id, listenerName, lastUpdatedUserKey));
    }

    public WebHookListenerRegistrationDetails getRegistrationDetails() {
        return registrationDetails;
    }

    public WebHookListenerParameters getParameters() {
        return params;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(registrationDetails, params);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final WebHookListener other = (WebHookListener) obj;
        return Objects.equal(this.registrationDetails, other.registrationDetails) && Objects.equal(this.params, other.params);
    }

    public static class Builder {
        private final WebHookListenerRegistrationDetails registrationDetails;
        private String url;
        private boolean excludeBody = false;
        private String filter = "";
        private List<String> propertyKeys = Collections.emptyList();

        private Builder(WebHookListenerRegistrationDetails registrationDetails) {
            this.registrationDetails = registrationDetails;
        }

        public Builder to(String url) {
            this.url = url;
            return this;
        }

        public WebHookListener build() {
            Preconditions.checkState(url != null);
            return new WebHookListener(registrationDetails, new WebHookListenerParameters(url, filter, excludeBody, propertyKeys));
        }

        public Builder excludeBody() {
            this.excludeBody = true;
            return this;
        }

        public Builder excludeBody(boolean exclude) {
            this.excludeBody = exclude;
            return this;
        }

        public Builder withFilter(String filter) {
            this.filter = filter;
            return this;
        }

        public Builder withPropertyKeys(List<String> propertyKeys) {
            this.propertyKeys = propertyKeys;
            return this;
        }
    }
}
