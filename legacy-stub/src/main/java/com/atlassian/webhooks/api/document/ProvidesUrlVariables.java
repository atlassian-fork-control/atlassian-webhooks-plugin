package com.atlassian.webhooks.api.document;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotate either a whole class implementing {@link com.atlassian.webhooks.spi.UriVariablesProvider} or its
 * method to indicate what variables it can provide. This information will be used in the UI.
 */
@Deprecated
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface ProvidesUrlVariables {
    String[] value();
}