package com.atlassian.webhooks.api.register.listener;

import com.google.common.base.Objects;
import com.google.common.base.Optional;
import io.atlassian.fugue.Option;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import static com.google.common.base.Preconditions.checkNotNull;
import static io.atlassian.fugue.Option.option;

/**
 * Details of web hook listener registration.
 * <p>
 * Webhook listeners can come from two sources: persistent stores or
 * module descriptors. This class can let you find out which one are you dealing with
 * and get some essential details of registation like the webhook id in persistent store,
 * or the key of plugin that registered it.
 */
@Deprecated
@Immutable
public final class WebHookListenerRegistrationDetails {
    private final ModuleDescriptorRegistrationDetails moduleDescriptorDetails;
    private final PersistentStoreRegistrationDetails persistentStoreDetails;
    private final WebHookListenerOrigin origin;

    private WebHookListenerRegistrationDetails(@Nullable ModuleDescriptorRegistrationDetails moduleDescriptorDetails, @Nullable PersistentStoreRegistrationDetails persistentStoreDetails, WebHookListenerOrigin origin) {
        this.moduleDescriptorDetails = moduleDescriptorDetails;
        this.persistentStoreDetails = persistentStoreDetails;
        this.origin = origin;
    }

    public static WebHookListenerRegistrationDetails persistentStore(@Nonnull Integer listenerId, @Nonnull String listenerName) {
        return persistentStore(listenerId, listenerName, null);
    }

    public static WebHookListenerRegistrationDetails persistentStore(@Nonnull Integer listenerId, @Nonnull String listenerName, Optional<String> lastUpdateUser) {
        return new WebHookListenerRegistrationDetails(null, new PersistentStoreRegistrationDetails(checkNotNull(listenerId), checkNotNull(listenerName), checkNotNull(lastUpdateUser)), WebHookListenerOrigin.PERSISTENT_STORE);
    }

    public static WebHookListenerRegistrationDetails moduleDescriptor(@Nonnull String plugninKey) {
        return new WebHookListenerRegistrationDetails(new ModuleDescriptorRegistrationDetails(checkNotNull(plugninKey)), null, WebHookListenerOrigin.MODULE_DESCRIPTOR);
    }

    /**
     * Details of plug-in that registered the listener.
     *
     * @return some details if the listener was registered by module descriptor, or absent if it comes from persistent store.
     */
    public Option<ModuleDescriptorRegistrationDetails> getModuleDescriptorDetails() {
        return option(moduleDescriptorDetails);
    }

    /**
     * Details of the persistently stored listener.
     *
     * @return some details if the listener comes from persistent store, or absent if it comes from module descriptor.
     */
    public Option<PersistentStoreRegistrationDetails> getPersistentStoreDetails() {
        return option(persistentStoreDetails);
    }

    /**
     * Returns the origin of the listener: persistent store or module descriptor.
     *
     * @return listener origin.
     */
    public WebHookListenerOrigin getOrigin() {
        return origin;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(moduleDescriptorDetails, persistentStoreDetails, origin);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final WebHookListenerRegistrationDetails other = (WebHookListenerRegistrationDetails) obj;
        return Objects.equal(this.moduleDescriptorDetails, other.moduleDescriptorDetails) && Objects.equal(this.persistentStoreDetails, other.persistentStoreDetails) && Objects.equal(this.origin, other.origin);
    }

    public static final class ModuleDescriptorRegistrationDetails {
        private final String pluginKey;

        private ModuleDescriptorRegistrationDetails(String pluginKey) {
            this.pluginKey = checkNotNull(pluginKey);
        }

        public String getPluginKey() {
            return pluginKey;
        }

        @Override
        public int hashCode() {
            return pluginKey.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof ModuleDescriptorRegistrationDetails &&
                    ((ModuleDescriptorRegistrationDetails) obj).getPluginKey().equals(getPluginKey());
        }
    }

    public static final class PersistentStoreRegistrationDetails {
        private final Integer id;
        private final String name;
        private final Optional<String> lastUpdatedUserKey;

        private PersistentStoreRegistrationDetails(Integer id, String name, Optional<String> lastUpdatedUserKey) {
            this.id = id;
            this.name = name;
            this.lastUpdatedUserKey = lastUpdatedUserKey;
        }

        public Integer getId() {
            return id;
        }

        public Optional<String> getLastUpdatedUserKey() {
            return lastUpdatedUserKey;
        }

        public String getName() {
            return name;
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(id, lastUpdatedUserKey, name);
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            final PersistentStoreRegistrationDetails other = (PersistentStoreRegistrationDetails) obj;
            return Objects.equal(this.id, other.id) &&
                    Objects.equal(this.lastUpdatedUserKey, other.lastUpdatedUserKey) &&
                    Objects.equal(this.name, other.name);
        }
    }
}
