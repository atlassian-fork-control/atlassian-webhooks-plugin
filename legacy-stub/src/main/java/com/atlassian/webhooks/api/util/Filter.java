package com.atlassian.webhooks.api.util;

import javax.annotation.concurrent.Immutable;

/**
 * An arbitrary String that can be used by {@link com.atlassian.webhooks.spi.EventMatcher}
 * to decide whether a specific  webhook can be published to a listener.
 * <p>
 * It's up to plug-ins that register webhooks to interpret filters in their matchers.
 */
@Deprecated
@Immutable
public final class Filter extends TypeRichString {
    public Filter(String value) {
        super(value);
    }
}
