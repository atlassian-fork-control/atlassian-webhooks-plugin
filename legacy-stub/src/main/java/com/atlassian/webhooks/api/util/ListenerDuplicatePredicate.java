package com.atlassian.webhooks.api.util;

import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.google.common.base.Predicate;

import javax.annotation.concurrent.Immutable;

@Deprecated
@Immutable
public class ListenerDuplicatePredicate implements Predicate<PersistentWebHookListener> {
    private final PersistentWebHookListener listener;

    public ListenerDuplicatePredicate(PersistentWebHookListener listener) {
        this.listener = listener;
    }

    public static Predicate<? super PersistentWebHookListener> duplicateOf(PersistentWebHookListener listener) {
        return new ListenerDuplicatePredicate(listener);
    }

    @Override
    public boolean apply(PersistentWebHookListener otherListener) {
        if (listener.getId().isDefined() && listener.getId().get().equals(otherListener.getId().get())) {
            return false; // can't be a duplicate of oneself
        }
        return listener.getEvents().equals(otherListener.getEvents()) && listener.getUrl().equals(otherListener.getUrl()) && listener.isExcludeBody() == otherListener.isExcludeBody()
                && listener.getFilters().equals(otherListener.getFilters());
    }
}
