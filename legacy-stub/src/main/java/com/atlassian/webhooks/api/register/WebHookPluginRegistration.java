package com.atlassian.webhooks.api.register;

import com.atlassian.webhooks.spi.*;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import io.atlassian.fugue.Option;

import javax.annotation.concurrent.Immutable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.transform;

/**
 * This class represent a self-sufficient set of webhook functionality
 * (events, serializers, variables providers...) registered by a plug-in.
 * <p>
 * Most webhook related stuff is registered by creating these objects, with a few exceptions
 * like {@link com.atlassian.webhooks.spi.RequestSigner} or {@link com.atlassian.webhooks.spi.WebHookListenerAccessVoter}
 */
@Deprecated
@Immutable
public class WebHookPluginRegistration {
    private final Map<Class, UriVariablesProvider> uriVariablesProviders;
    private final Map<Class, EventSerializer> eventSerializers;
    private final Option<WebHookPluginRegistrationFactory.CloudCondition> cloudCondition;
    private final List<WebHookEventSection> sections;
    private final List<WebHooksHtmlPanel> panels;
    private final Set<RequestSigner> requestSigners;
    private final Set<WebHookListenerActionValidator> validators;
    private final Set<RequestSigner2> requestSigners2;
    private final Set<QueryParamsProvider> queryParamsProviders;

    WebHookPluginRegistration(Map<Class, UriVariablesProvider> uriVariablesProviders,
                              Map<Class, EventSerializer> serializers,
                              Option<WebHookPluginRegistrationFactory.CloudCondition> cloudCondition,
                              Iterable<WebHookEventSection> sections,
                              List<WebHooksHtmlPanel> panels,
                              Set<RequestSigner> requestSigners,
                              Set<RequestSigner2> requestSigners2,
                              Set<WebHookListenerActionValidator> validators,
                              Set<QueryParamsProvider> queryParamsProviders) {
        this.requestSigners2 = requestSigners2;
        this.queryParamsProviders = queryParamsProviders;
        this.requestSigners = ImmutableSet.copyOf(requestSigners);
        this.validators = ImmutableSet.copyOf(validators);
        this.sections = ImmutableList.copyOf(sections);
        this.panels = ImmutableList.copyOf(panels);
        this.uriVariablesProviders = ImmutableMap.copyOf(uriVariablesProviders);
        this.cloudCondition = cloudCondition;
        this.eventSerializers = ImmutableMap.copyOf(serializers);
    }

    public static WebHookPluginRegistrationBuilder builder() {
        return new WebHookPluginRegistrationBuilder();
    }

    public Map<Class, UriVariablesProvider> getUriVariablesProviders() {
        return uriVariablesProviders;
    }

    public Map<Class, EventSerializer> getEventSerializers() {
        return eventSerializers;
    }

    public Option<WebHookPluginRegistrationFactory.CloudCondition> getCloudCondition() {
        return cloudCondition;
    }

    public List<WebHookEventSection> getSections() {
        return sections;
    }

    private Iterable<WebHookEventGroup> getGroups() {
        return concat(transform(sections, new Function<WebHookEventSection, Set<WebHookEventGroup>>() {
            @Override
            public Set<WebHookEventGroup> apply(final WebHookEventSection section) {
                return section.getGroups();
            }
        }));
    }

    public Iterable<RegisteredWebHookEvent> getRegistrations() {
        return concat(transform(getGroups(), new Function<WebHookEventGroup, Iterable<RegisteredWebHookEvent>>() {
            @Override
            public Iterable<RegisteredWebHookEvent> apply(final WebHookEventGroup group) {
                return group.getEvents();
            }
        }));
    }

    public List<WebHooksHtmlPanel> getPanels() {
        return panels;
    }

    public Set<WebHookListenerActionValidator> getValidators() {
        return validators;
    }

    public Set<RequestSigner> getRequestSigners() {
        return requestSigners;
    }

    public Set<RequestSigner2> getRequestSigners2() {
        return requestSigners2;
    }

    public Set<QueryParamsProvider> getQueryParamsProviders() {
        return queryParamsProviders;
    }
}
