package com.atlassian.webhooks.spi.provider;

@Deprecated
public interface EventBuilder {
    <E> MapperBuilder<E> whenFired(Class<E> eventClass);
}
