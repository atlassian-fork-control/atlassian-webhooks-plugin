package com.atlassian.webhooks.spi;

import com.atlassian.webhooks.api.publish.WebHookEvent;
import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.register.listener.WebHookListener;
import com.atlassian.webhooks.api.util.Channel;
import com.atlassian.webhooks.api.util.Vote;

import javax.annotation.Nonnull;

/**
 * Plugin point for authorising webhook interactions. Plugins that want to control who can create, list and administer
 * webhooks should implement this interface and publish it as an OSGI service.
 * <p>
 * When a webhook interaction is requested, each access voter is asked to cast a {@link com.atlassian.webhooks.api.util.Vote vote}, which can be either
 * {@link com.atlassian.webhooks.api.util.Vote#ALLOW}, {@link com.atlassian.webhooks.api.util.Vote#DENY} or {@link com.atlassian.webhooks.api.util.Vote#ABSTAIN}. The votes are interpreted as follows:
 * <ul>
 * <li>If at least one voter denies access, access is denied</li>
 * <li>If no voter denies access and at least one voter allows access, access is granted</li>
 * <li>If all voters abstain, access is denied</li>
 * </ul>
 * The system ships with a default access voter that applies the following logic:
 * <ul>
 * <li>for the {@link com.atlassian.webhooks.api.util.Channel#SERVICE service channel}, access is always granted</li>
 * <li>for the other channels, access is granted if the user is an administrator</li>
 * <li>in all other cases, the default voter {@link com.atlassian.webhooks.api.util.Vote#ABSTAIN abstains}</li>
 * </ul>
 */
@Deprecated
public interface WebHookListenerAccessVoter {
    /**
     * Called to validate whether the current user can create a new webhook registration.
     * <p>
     * Note: Implementations should handle all exceptions. Failure to do so will prevent the webhook from being
     * dispatched.
     *
     * @param parameters the registration parameters
     * @param channel    the channel over which the request is made
     * @return the vote
     */
    @Nonnull
    Vote canCreate(@Nonnull PersistentWebHookListener parameters, @Nonnull Channel channel);

    /**
     * Called to validate whether the current user can see the webhook registration. This method is called whenever one
     * or more webhooklisteners are retrieved.
     * <p>
     * Note: Implementations should handle all exceptions. Failure to do so will prevent the webhook from being
     * dispatched.
     *
     * @param parameters the webhook listener parameters
     * @param channel    the channel over which the request is made
     * @return the vote
     */
    @Nonnull
    Vote canRead(@Nonnull PersistentWebHookListener parameters, @Nonnull Channel channel);

    /**
     * Called to validate whether the current user can administer the webhook registration. This method is called when
     * an attempt is made to update or delete a webhooklistener.
     * <p>
     * Note: Implementations should handle all exceptions. Failure to do so will prevent the webhook from being
     * dispatched.
     *
     * @param parameters the webhook listener parameters
     * @param channel    the channel over which the request is made
     * @return the vote
     */
    @Nonnull
    Vote canAdmin(@Nonnull PersistentWebHookListener parameters, @Nonnull Channel channel);

    /**
     * Called to validate whether a webhook event can be publised to a webhook listener. The default voter will always
     * vote ALLOW.
     * <p>
     * Note: Implementations should handle all exceptions. Failure to do so will prevent the webhook from being
     * dispatched.
     *
     * @param webHookEvent the event that's being published
     * @param listener     the intended receiver
     * @return the vote
     */
    @Nonnull
    Vote canPublish(@Nonnull WebHookEvent webHookEvent, @Nonnull WebHookListener listener);
}
