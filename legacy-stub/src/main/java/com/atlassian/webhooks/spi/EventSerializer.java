package com.atlassian.webhooks.spi;

import com.atlassian.webhooks.api.util.WebhookSerializerParameters;

/**
 * Serializes events of the specified type to JSON.
 *
 * @param <E> event type
 */
@Deprecated
public interface EventSerializer<E> {
    /**
     * @deprecated This method is never called. {@link EventSerializer#serialize(Object, WebhookSerializerParameters)}
     * should be implemented instead.
     */
    @Deprecated
    String serialize(E event);

    default String serialize(E event, WebhookSerializerParameters webhookParameters) {
        return serialize(event);
    }
}
