package com.atlassian.webhooks.spi;

/**
 * Function that returns arbitrary HTML fragments. Used by plug-ins
 * to provide additional panels in webhooks admin interface.
 */
@Deprecated
public interface WebHooksHtmlPanel {
    String getHtml();
}
