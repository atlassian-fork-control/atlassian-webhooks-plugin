package com.atlassian.webhooks.spi.provider;

@Deprecated
public interface EventSerializerFactory<T> {
    EventSerializer create(T event);
}
