package com.atlassian.webhooks.spi;

import com.atlassian.webhooks.api.register.listener.PersistentWebHookListener;
import com.atlassian.webhooks.api.register.listener.RegistrationMethod;
import io.atlassian.fugue.Option;

import java.util.Collection;

/**
 * Store to be implemented by products.
 */
@Deprecated
public interface WebHookListenerStore {
    /**
     * Adds a new WebHook listener and returns the newly created WebHook listener.
     *
     * @param listener           parameters of the listener.
     * @param registrationMethod REST, UI or SERVICE.
     */
    PersistentWebHookListener addWebHook(PersistentWebHookListener listener, RegistrationMethod registrationMethod);

    /**
     * Updates existing WebHook listener and returns the newly created WebHook.
     *
     * @param listener parameters of the listener.
     */
    PersistentWebHookListener updateWebHook(PersistentWebHookListener listener);

    /**
     * Get a single WebHook Listener by id.
     *
     * @param id of the WebHook Listener.
     * @return the WebHook listener.
     */
    Option<PersistentWebHookListener> getWebHook(int id);

    /**
     * Removes single WebHook Listener by id.
     *
     * @param id of the WebHook Listener.
     * @throws IllegalArgumentException the specified id does not exist
     */
    void removeWebHook(int id);

    /**
     * Get a list of all listeners in the system.
     *
     * @return collection of WebHook listeners.
     */
    Collection<PersistentWebHookListener> getAllWebHooks();
}
