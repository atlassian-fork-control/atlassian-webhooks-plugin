package com.atlassian.webhooks.spi.plugin;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.webhooks.spi.provider.ModuleDescriptorWebHookListenerRegistry;
import com.atlassian.webhooks.spi.provider.PluginModuleListenerParameters;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Element;

import javax.annotation.Nonnull;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkNotNull;

@Deprecated
public class WebHookModuleDescriptor extends AbstractModuleDescriptor<Void> {
    public static final String PROPERTY_KEYS = "propertyKeys";

    private final ModuleDescriptorWebHookListenerRegistry webHookListenerRegistry;

    private String eventIdentifier;
    private URI url;
    private String moduleKey;
    private Map<String, Object> moduleParams;
    private String webhookPluginKey;

    public WebHookModuleDescriptor(ModuleFactory moduleFactory, ModuleDescriptorWebHookListenerRegistry webHookListenerRegistry) {
        super(moduleFactory);
        this.webHookListenerRegistry = checkNotNull(webHookListenerRegistry);
    }

    private static String getOptionalAttribute(Element e, String name, Object defaultValue) {
        String value = e.attributeValue(name);
        return value != null ? value :
                defaultValue != null ? defaultValue.toString() : null;
    }

    private static URI getRequiredUriAttribute(Element e, String name) {
        String value = getRequiredAttribute(e, name);
        return URI.create(value);
    }

    private static String getRequiredAttribute(Element e, String name) {
        String value = e.attributeValue(name);
        if (value == null) {
            throw new PluginParseException("Attribute '" + name + "' is required on '" + e.getName() + "'");
        }
        return value;
    }

    @Override
    public Void getModule() {
        return null;
    }

    @Override
    public void init(@Nonnull Plugin plugin, @Nonnull Element element) throws PluginParseException {
        super.init(plugin, element);
        eventIdentifier = getOptionalAttribute(element, "event", getKey());
        url = getRequiredUriAttribute(element, "url");
        moduleKey = getRequiredAttribute(element, "key");
        final ImmutableMap.Builder<String, Object> params = new ImmutableMap.Builder<String, Object>();

        List<Element> elements = element.elements("param");
        if (elements != null) {
            elements.stream()
                    .filter(e -> !getRequiredAttribute(e, "name").equals("propertyKey"))
                    .forEach(e -> params.put(getRequiredAttribute(e, "name"), e.getText()));

            List<String> propertyKeys = elements.stream()
                    .filter(e -> getRequiredAttribute(e, "name").equals("propertyKey"))
                    .map(e -> e.getText())
                    .collect(Collectors.toList());

            params.put(PROPERTY_KEYS, propertyKeys);
        }
        moduleParams = params.build();
    }

    @Override
    public void enabled() {
        super.enabled();
        webHookListenerRegistry.register(eventIdentifier, getWebhookPluginKey(), url, new PluginModuleListenerParameters(getWebhookPluginKey(), Optional.of(moduleKey), moduleParams, eventIdentifier));
    }

    @Override
    public void disabled() {
        webHookListenerRegistry.unregister(eventIdentifier, getWebhookPluginKey(), url, new PluginModuleListenerParameters(getWebhookPluginKey(), Optional.of(moduleKey), moduleParams, eventIdentifier));
        super.disabled();
    }

    public String getWebhookPluginKey() {
        return (StringUtils.isNotBlank(webhookPluginKey)) ? webhookPluginKey : getPluginKey();
    }

    /**
     * allows for overriding the plugin key used for the actual webhook registration.
     * e.g. the pluginKey registered may be different than the plugin the moduel descriptor is registered to.
     * This is the case for all connect addons where the moduel descriptor is registered to the connect plugin
     * but the webhook needs to be registered with the addon's key
     *
     * @param webhookPluginKey the plugin key
     */
    public void setWebhookPluginKey(String webhookPluginKey) {
        this.webhookPluginKey = webhookPluginKey;
    }
}
