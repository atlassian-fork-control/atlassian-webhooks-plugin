<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>com.atlassian.webhooks</groupId>
        <artifactId>atlassian-webhooks-parent</artifactId>
        <version>6.1.1-SNAPSHOT</version>
    </parent>

    <artifactId>atlassian-webhooks-legacy-stub</artifactId>
    <packaging>atlassian-plugin</packaging>

    <!-- Bundle that provides the types from the v3 API and SPI, without any implementation. This bundle
         exists only to allow a host application to provide the old types over OSGI, which may be required for to
         allow plugins that still rely on the old API/SPI to start.
    -->

    <name>Atlassian Webhooks Legacy API/SPI Stub</name>
    <description>Legacy API/SPI (v3) Stub for Atlassian Webhooks</description>

    <dependencies>
        <dependency>
            <groupId>com.atlassian.sal</groupId>
            <artifactId>sal-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.event</groupId>
            <artifactId>atlassian-event</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.httpclient</groupId>
            <artifactId>atlassian-httpclient-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-core</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>io.atlassian.fugue</groupId>
            <artifactId>fugue</artifactId>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.google.code.findbugs</groupId>
            <artifactId>jsr305</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.json</groupId>
            <artifactId>json</artifactId>
            <version>20090211</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-osgi</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>javax.inject</groupId>
            <artifactId>javax.inject</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>javax.validation</groupId>
            <artifactId>validation-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.hibernate.validator</groupId>
            <artifactId>hibernate-validator</artifactId>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>amps-maven-plugin</artifactId>
                <configuration>
                    <extractDependencies>false</extractDependencies>
                    <containerId>tomcat85x</containerId>
                    <instructions>
                        <Atlassian-Plugin-Key>${project.groupId}.${project.artifactId}</Atlassian-Plugin-Key>
                        <Private-Package>
                            com.atlassian.webhooks.internal*
                        </Private-Package>
                        <Import-Package>
                            <!-- Import the webhooks packages in case the host application exports the
                                 webhooks API/SPI -->
                            com.atlassian.webhooks*;version="${project.version}",
                            com.atlassian.analytics.*;resolution:="optional",
                            com.atlassian.plugin.osgi.bridge.external;version="${plug.osgi.version}",
                            com.atlassian.plugin.osgi.external;version="${plug.osgi.version}",
                            org.apache.commons.io.input;version="2.6",
                            org.springframework.osgi.*;resolution:="optional",
                            org.hibernate.validator.*;resolution:="optional",
                            org.jboss.logging*;resolution:="optional",
                            org.osgi.*,
                            javax.annotation;resolution:="optional",
                            *
                        </Import-Package>
                        <Export-Package>
                            com.atlassian.webhooks.api*;version="${project.version}",
                            com.atlassian.webhooks.spi*;version="${project.version}",
                        </Export-Package>
                        <!-- Ensure plugin is spring powered - see https://extranet.atlassian.com/x/xBS9hQ -->
                        <Spring-Context>*</Spring-Context>
                    </instructions>
                </configuration>
            </plugin>

        </plugins>
    </build>

    <properties>
        <atlassian.spring.scanner.version>1.2.13</atlassian.spring.scanner.version>
        <!-- This key is used to keep the consistency between the key in atlassian-plugin.xml and the key to generate bundle. -->
        <atlassian.plugin.key>${project.groupId}.${project.artifactId}</atlassian.plugin.key>
    </properties>

</project>
