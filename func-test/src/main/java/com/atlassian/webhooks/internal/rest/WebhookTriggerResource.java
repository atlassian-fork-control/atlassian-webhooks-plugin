package com.atlassian.webhooks.internal.rest;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.webhooks.*;
import com.atlassian.webhooks.internal.refapp.MoonBaseCreatedEvent;
import com.atlassian.webhooks.internal.refapp.ProjectCreatedEvent;
import com.atlassian.webhooks.internal.refapp.RefappScope;
import com.atlassian.webhooks.request.WebhookHttpRequest;
import com.atlassian.webhooks.request.WebhookHttpResponse;
import com.sun.jersey.spi.resource.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static com.atlassian.webhooks.internal.refapp.RefappEvent.MOONBASE_CREATED;
import static com.atlassian.webhooks.internal.refapp.RefappEvent.PROJECT_CREATE;

@AnonymousAllowed
@Consumes(MediaType.APPLICATION_JSON)
@Path("triggers")
@Produces(MediaType.APPLICATION_JSON)
@Singleton
public class WebhookTriggerResource {

    private static final Logger log = LoggerFactory.getLogger(WebhookTriggerResource.class);

    private final Map<String, WebhookTestStatistics> statistics;
    private final WebhookService webhookService;

    public WebhookTriggerResource(WebhookService webhookService) {
        this.webhookService = webhookService;

        statistics = new ConcurrentHashMap<>();
    }

    @GET
    @Path("{call-id}/statistics")
    public Response getCallStatistics(@PathParam("call-id") String id) {
        WebhookTestStatistics stats = statistics.get(id);
        if (stats == null) {
            return Response.status(404).build();
        }
        return Response.ok(stats).build();
    }


    @POST
    @Path(("/moon-bases/{key}/create"))
    public Response triggerMoonBaseCreated(@PathParam("key") String key) {
        // The MoonBaseCreatedEvent (and moon-base:created event) is used to test dispatches to webhooks registered
        // through a <webhook> module in atlassian-plugin.xml. Since <webhook> modules cannot easily be disabled, the
        // tests for verifying dispatches to webhook listeners registered through the API and atlassian-plugin.xml use
        // different events to be independent.
        LatchingCallback callback = new LatchingCallback();
        webhookService.publish(WebhookPublishRequest.builder(MOONBASE_CREATED, new MoonBaseCreatedEvent(key))
                .scopes(new RefappScope("moon-base", key))
                .callback(callback)
                .build());

        try {
            if (callback.await(5, TimeUnit.SECONDS)) {
                return Response.ok().build();
            }
        } catch (InterruptedException e) {
            return Response.status(500).entity(e.getMessage()).build();
        }
        return Response.status(500).entity("dispatch timed out").build();
    }

    /**
     * Triggers webhooks for the project:create event and scope (project, projectKey) to be dispatched
     *
     * @param projectKey the project key
     * @return a call ID which can be used to retrieve statistics
     */
    @POST
    @Path(("/projects/{projectKey}/create"))
    public Response triggerProjectCreated(@PathParam("projectKey") String projectKey,
                                          @QueryParam("count") @DefaultValue("1") int count) {
        // The ProjectCreatedEvent (and project:created event) is used to test dispatches to webhooks registered
        // through the API. Since <webhook> modules cannot easily be disabled, the tests for verifying dispatches
        // to webhook listeners registered through the API and atlassian-plugin.xml use different events to be
        // independent.
        String uuid = UUID.randomUUID().toString();
        WebhookTestStatistics testStatistics = new WebhookTestStatistics();
        statistics.put(uuid, testStatistics);

        WebhookCallback statisticsCallback = new StatisticsRecordingCallback(testStatistics);

        new Thread(() -> {
            for (int i = 0; i < count; ++i) {
                try {
                    webhookService.publish(WebhookPublishRequest.builder(PROJECT_CREATE, new ProjectCreatedEvent(projectKey))
                            .scopes(WebhookScope.GLOBAL, new RefappScope("project", projectKey))
                            .callback(statisticsCallback)
                            .build());
                } catch (RuntimeException e) {
                    testStatistics.onDispatchFailure();
                }
            }
        }).start();

        return Response.ok(uuid).build();
    }

    private static class LatchingCallback implements WebhookCallback {

        private final CountDownLatch latch = new CountDownLatch(1);

        public boolean await(long timeout, TimeUnit unit) throws InterruptedException {
            return latch.await(timeout, unit);
        }

        @Override
        public void onError(WebhookHttpRequest request, @Nonnull Throwable error, @Nonnull WebhookInvocation webhook) {
            latch.countDown();
        }

        @Override
        public void onFailure(@Nonnull WebhookHttpRequest request, @Nonnull WebhookHttpResponse response,
                              @Nonnull WebhookInvocation webhook) {
            latch.countDown();
        }

        @Override
        public void onSuccess(@Nonnull WebhookHttpRequest request, @Nonnull WebhookHttpResponse response,
                              @Nonnull WebhookInvocation webhook) {
            latch.countDown();
        }
    }

    private static class StatisticsRecordingCallback implements WebhookCallback {

        private final WebhookTestStatistics statistics;

        private StatisticsRecordingCallback(WebhookTestStatistics statistics) {
            this.statistics = statistics;
        }

        @Override
        public void onError(WebhookHttpRequest request, @Nonnull Throwable error, @Nonnull WebhookInvocation webhook) {
            log.error("Error dispatching webhook: {}", error.getMessage());
            statistics.onError();
        }

        @Override
        public void onFailure(@Nonnull WebhookHttpRequest request, @Nonnull WebhookHttpResponse response,
                              @Nonnull WebhookInvocation webhook) {
            statistics.onFailure();
        }

        @Override
        public void onSuccess(@Nonnull WebhookHttpRequest request, @Nonnull WebhookHttpResponse response,
                              @Nonnull WebhookInvocation webhook) {
            statistics.onSuccess();
        }
    }
}
