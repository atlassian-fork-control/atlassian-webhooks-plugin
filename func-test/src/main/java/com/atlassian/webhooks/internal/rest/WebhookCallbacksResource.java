package com.atlassian.webhooks.internal.rest;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.sun.jersey.spi.resource.Singleton;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.concurrent.atomic.AtomicInteger;

@AnonymousAllowed
@Consumes(MediaType.APPLICATION_JSON)
@Path("callbacks")
@Produces(MediaType.APPLICATION_JSON)
@Singleton
public class WebhookCallbacksResource {

    private final AtomicInteger counter;

    public WebhookCallbacksResource() {
        counter = new AtomicInteger();
    }

    @DELETE
    public Response clearStatistics() {
        counter.set(0);

        return Response.ok().build();
    }

    @GET
    public Response getCallbackCounts() {
        return Response.ok(counter.get()).build();
    }

    @POST
    public Response webhookCallback() {
        int count = counter.incrementAndGet();
        return Response.ok(count).build();
    }
}
