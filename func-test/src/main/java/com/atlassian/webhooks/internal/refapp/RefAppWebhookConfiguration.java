package com.atlassian.webhooks.internal.refapp;

import com.atlassian.webhooks.WebhooksConfiguration;

import javax.annotation.Nonnull;
import java.time.Duration;
import java.time.temporal.ChronoUnit;

public class RefAppWebhookConfiguration implements WebhooksConfiguration {

    @Override
    public int getMaxInFlightDispatches() {
        return 100_000;
    }

    @Nonnull
    @Override
    public Duration getDispatchTimeout() {
        return Duration.of(10, ChronoUnit.SECONDS);
    }
}
