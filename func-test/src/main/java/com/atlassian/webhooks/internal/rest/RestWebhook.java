package com.atlassian.webhooks.internal.rest;

import com.atlassian.webhooks.Webhook;
import com.atlassian.webhooks.WebhookEvent;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings("unchecked")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class RestWebhook extends LinkedHashMap<String, Object> {

    public RestWebhook() {
    }

    public RestWebhook(Webhook webhook) {
        put("id", webhook.getId());
        put("name", webhook.getName());
        put("url", webhook.getUrl());
        put("active", webhook.isActive());
        if (!webhook.getConfiguration().isEmpty()) {
            put("configuration", webhook.getConfiguration());
        }
        put("createdDate", webhook.getCreatedDate());
        put("events", webhook.getEvents().stream().map(WebhookEvent::getId).collect(Collectors.toList()));
        put("scope", new RestWebhookScope(webhook.getScope()));
    }

    public Map<String, String> getConfiguration() {
        Object value = get("configuration");
        if (value instanceof Map) {
            return (Map<String, String>) value;
        }
        return Collections.emptyMap();
    }

    public List<String> getEvents() {
        Object value = get("events");
        if (value instanceof List) {
            return (List<String>) value;
        }
        return Collections.emptyList();
    }

    public int getId() {
        return (Integer) get("id");
    }

    public String getName() {
        return (String) get("name");
    }

    public RestWebhookScope getScope() {
        return RestWebhookScope.valueOf(get("scope"));
    }

    public String getUrl() {
        return (String) get("url");
    }

    public boolean isActive() {
        return (Boolean) get("active");
    }
}
