package com.atlassian.webhooks.internal.refapp;

/**
 * An example event used for testing of webhook dispatches
 */
public class ProjectCreatedEvent {
    private final String projectKey;

    public ProjectCreatedEvent(String projectKey) {
        this.projectKey = projectKey;
    }

    public String getProjectKey() {
        return projectKey;
    }
}
