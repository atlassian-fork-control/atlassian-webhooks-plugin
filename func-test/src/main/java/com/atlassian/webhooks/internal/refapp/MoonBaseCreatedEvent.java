package com.atlassian.webhooks.internal.refapp;

/**
 * An example event used for testing of webhook dispatches
 */
public class MoonBaseCreatedEvent {

    private final String key;

    public MoonBaseCreatedEvent(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}
